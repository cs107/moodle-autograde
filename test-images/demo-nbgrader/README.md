# Autograded Jupyter notebook

The purpose of this document is to go through the steps of creating a Jupyter notebook homework graded with [nbgrader](https://nbgrader.readthedocs.io/en/stable/), and containerzing the process for use with the Moodle Autograde plugin.

The following tutorial can be followed either with a local instance of Jupyter server or directly on the noto plateform. When encountering details options like bellow, please unfold the details for the infrastructure your are following the tutoriel on.

<details>
<summary>Local server</summary>
Unfold the local server details, to find instructions and informations for the local server.
</details>
<details>
<summary>Noto</summary>
Unfold the noto details, to find instructions and informations for Noto.
</details>





## 0. Access Jupyter Server


<details>
<summary>Local server</summary>

Build the Jupyter server with nbgrader plugin included: 
```bash
docker build . -f Dockerfile.local_jupyter_server -t local_jupyter_server
```

Start the Jupyter server: 
```bash
docker run -it -p 8888:8888 -v ./nbgrader_workspace:/nbgrader_workspace local_jupyter_server
```

The previous command runs until ended by `CTRL + C`. It produces textual output among which the server url and authentication token after `Or copy and paste one of these URLs`. Copy the second url, and access it in your favorite webbrower. 

</details>

<details>
<summary>Noto</summary>

Access noto at [noto.epfl.ch](https://noto.epfl.ch/)
</details>

## 1. Create an assignment

Go to formgrader, `nbgrader > formgrader`. Formgrader is the interface where user can manage existing assignments and create new ones.

![formgrader](images/formgrader.png)


<details>
<summary>Noto</summary>

If you cannot access the formgrader and encounter the error `refused to connect`, go to the noto command terminal:
![noto terminal](images/noto_terminal.png)

And execute the following instructions: 
```
cd ~/.jupyter
echo "c.ServerApp.tornado_settings = {'headers': {'Content-Security-Policy': \"frame-ancestors 'self';\"}}" >> jupyter_server_config.py
```

After this you will have to logout for the changes to be effective.
</details>

Create a new assignment with `+ Add new assignment`.

![add new assignment](images/add_new_assignment.png)

The assignment name is mandatory. Howewher the due date and timezone are optionals. 

![assignment details](images/assignment_informations.png)

You should now see the newly created assignment in the list of existing assignments.

![assignment list](images/assignment_list.png)


<details>
<summary>Local server</summary>
Click on the assignment name to access the assignment workspace.

In the assignment worspace you can create Jupyter notebook homeworks with `new > Python 3 (ipykernel)`.

![new jupyter notebook](images/new_jupyter_notebook.png)
</details>

<details>
<summary>Noto</summary>

Click on the assignment name to access the assignment workspace.

This will keep the same formgrader window open, but will change the current working directory of the folder explorer. 

In the file explorer you can create a new notebook file (right click > New notebook). This creates a notebook and displays it. When prompted for a kernel, choose `Python3`.
</details>

You can make the notebook to be autogradable `nbgrader > Create assignment`. 

![create assignment](images/create_assignment.png)

This displays a configuration pannel with possible annotations next to each notebook cells. 
- mark cells that requires to be filled by students as `Autograded answers`
- mark cells containing autograded tests as `Autograded tests`

![cells annotations](images/cell_annotations.png)

The idea when creating autograded assignments it to start by creating the homework with solutions. Nbgrader can afterward generate the student version. Il replaces solutions between special meta comments as bellow with a placeholder in the student version:

![meta_comment](images/meta_comment.png)

Nbgrader test cells are in the form of `assert` verifications to grade the code:

![assert](images/assert.png)

When the homework is finished, you can verify the adequation of the expected solution / provided tests with the `validate` button.

![verify](images/validate.png)

### 2. Release the assignment

Releasing the assignment is the process of building the student version of the assignment.

Go to the formgrader `nbgrader > formgrader`. 

From the list of assignment, find the one you want to release and than click on the `generate` symbol. The resulting files can be forwarded to students as their homework. 

![generate](images/generate_assignment.png)

<details>
<summary>Local server</summary>

The student version of the assignment should be available in `./nbgrader_workspace/course/release` on your machine.
</details>

<details>
<summary>Noto</summary>

The student version of the assignment should be available in `~/release/<assignment_name>`.
</details>



### 3. Build the autograding image

<details>
<summary>Local server</summary>

Make sure to set the variable `ASSIGNMENT_NAME` to your assignment name in `./entrypoints/autograde_assignment.sh`.

Build the grading image: 
```bash
docker build . -f Dockerfile.assignment_autograder -t assignment_autograder
```
</details>

<details>
<summary>Noto</summary>

Import the following file and folder to your noto user home directory:
- Dockerfile.assignment_autograder_noto
- ./entrypoints

Make sure to set the variable `ASSIGNMENT_NAME` to your assignment name in both `./entrypoints/autograde_assignment.sh` and in `Dockerfile.assignment_autograder_noto`.

In noto terminal, build the grading image:
```
cd ~
DOCKER_BUILDKIT=1 DOCKER_HOST=ssh://grader@192.168.130.160 docker build . -f Dockerfile.assignment_autograder_noto -t assignment_autograder
```

The previous command requires you to ask the [noto team](mailto:noto-support@groupes.epfl.ch) access to the docker runtime from your noto account.

</details>
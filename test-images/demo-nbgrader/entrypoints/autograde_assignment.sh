#!/bin/sh
ASSIGNMENT_NAME="your_assignment_name_here"
COURSE_NAME="course"
STUDENT_ID="student"

echo "[+] starting the autograde of the assignment"
cd "/nbgrader_workspace/$COURSE_NAME"
echo "[+] autograding $ASSIGNMENT_NAME"

## the student submission has to be moved from /data/submission to 
## /nbgrader_workspace/<course_name>/<student_id>/<assignment_name>/ 
## so that nbgrader can access the submission
mkdir -p "submitted/$STUDENT_ID/$ASSIGNMENT_NAME"
cp -a /data/submission/. "submitted/$STUDENT_ID/$ASSIGNMENT_NAME/"

## grade and produce feedback
nbgrader autograde "$ASSIGNMENT_NAME"
nbgrader generate_feedback "$ASSIGNMENT_NAME"
nbgrader export

## export the grade and the feedback to /data/feedback
score=$(awk -F',' 'NR==2 {print $10}' grades.csv)
echo "score: $score"
cd /data/feedback
if [ -z "$score" ]; then 
    echo "{\"grade\": \"0\"}" > grade.json
else
    echo "{\"grade\": \"$score\"}" > grade.json
fi
ls /nbgrader_workspace/$COURSE_NAME/feedback/$STUDENT_ID/$ASSIGNMENT_NAME/
cp -a "/nbgrader_workspace/$COURSE_NAME/feedback/$STUDENT_ID/$ASSIGNMENT_NAME/." /data/feedback
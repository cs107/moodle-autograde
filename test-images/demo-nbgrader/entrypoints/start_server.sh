#!/bin/sh
COURSE_NAME="course"

echo "[+] startig the jupyter server with the nbgrader plugin..."
cd /nbgrader_workspace

nbgrader quickstart "$COURSE_NAME"
cd "./$COURSE_NAME"
jupyter notebook --allow-root --ip 0.0.0.0 --port 8888 --no-browser
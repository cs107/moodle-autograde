package ch.epfl.autograde.demo.java;

import io.github.hamzaremmal.jgrade.api.JGradeTask;
import io.github.hamzaremmal.jgrade.api.JGradeTest;
import io.github.hamzaremmal.jgrade.api.Random;
import io.github.hamzaremmal.jgrade.api.assertions.JGradeBasicAssertions;

/**
 * JGrade Task to grade the content of {@link Math}
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.0.0
 */
@JGradeTask(name = "Implementation of Math.java", order = 1)
public final class MathTests {

    @JGradeTask(name = "Implementation of Math::add", order = 1)
    public static final class AdditionTest {

        private final JGradeBasicAssertions basic;

        public AdditionTest(JGradeBasicAssertions basic) {
            this.basic = basic;
        }

        @JGradeTest(name = "Correct behavior of Math::add", score = 1)
        public void correctness(@Random int a, @Random int b){
            basic.assertEquals(a + b, Math.add(a, b));
        }

    }

    @JGradeTask(name = "Implementation of Math::sub", order = 2)
    public static final class SubtractionTest {

        private final JGradeBasicAssertions basic;

        public SubtractionTest(JGradeBasicAssertions basic) {
            this.basic = basic;
        }

        @JGradeTest(name = "Correct behavior of Math::sub", score = 1)
        public void correctness(@Random int a, @Random int b){
            basic.assertEquals(a - b, Math.sub(a, b));
        }

    }

    @JGradeTask(name = "Implementation of Math::mul", order = 3)
    public static final class MultiplicationTest {

        private final JGradeBasicAssertions basic;

        public MultiplicationTest(JGradeBasicAssertions basic) {
            this.basic = basic;
        }

        @JGradeTest(name = "Correct behavior of Math::mul", score = 1)
        public void correctness(@Random int a, @Random int b){
            basic.assertEquals(a * b, Math.mul(a, b));
        }

    }

    @JGradeTask(name = "Implementation of Math::div", order = 4)
    public static final class DivisionTest {

        private final JGradeBasicAssertions basic;

        public DivisionTest(JGradeBasicAssertions basic) {
            this.basic = basic;
        }

        @JGradeTest(name = "Correct behavior of Math::div", score = 1)
        public void correctness(@Random int a, @Random int b){
            basic.assertEquals(a / b, Math.div(a, b));
        }

        @JGradeTest(name = "Math::div throws IllegalArgumentException when dividing by 0", score = 1)
        public void divByZero(@Random int a){
            basic.assertThrows(IllegalArgumentException.class, () -> Math.div(a, 0));
        }

    }

}

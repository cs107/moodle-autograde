package ch.epfl.autograde.demo.java;

/**
 * Simple utility class to perform simple mathematical operations
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.0.0
 */
public final class Math {

    private Math(){}

    /**
     * Performs the addition on integers
     * @param a lhs operand
     * @param b rhs operand
     * @return the addition of both operands
     */
    public static int add(int a, int b){
        return a + b;
    }

    /**
     * Performs the subtraction on integers
     * @param a lhs operand
     * @param b rhs operand
     * @return the subtraction of both operands
     */
    public static int sub(int a, int b){
        return a - b;
    }

    /**
     * Performs the multiplication on integers
     * @param a lhs operand
     * @param b rhs operand
     * @return the multiplication of both operands
     */
    public static int mul(int a, int b){
        return a * b;
    }

    /**
     * Performs the division on integers
     * @param a lhs operand
     * @param b rhs operand
     * @return the division of both operands
     */
    public static int div(int a, int b){
        if(b == 0) throw new IllegalArgumentException();
        return a / b;
    }

}

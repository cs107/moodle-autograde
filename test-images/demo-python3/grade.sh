#!/bin/sh

save_feedback() {
    grade=$1
    feedback=$2

    # Feedback must be saved in `/data/feedback`.
    # - `grade.json`, must be a JSON file with {"grade": grade_as_a_number}
    # - other file are attached as feedback files
    echo "[*] Saving grade $grade and feedback \"$feedback\""
    echo "{\"grade\": $grade}" > /data/feedback/grade.json
    echo "$feedback" > /data/feedback/feedback.txt
    exit 0
}

grade() {
    # Files submitted by the student are mounted in `/data/submission`.

    if [ ! -f /data/submission/script.py ]; then
        save_feedback 0 "I have not found the script.py file :'("
    fi

    # Run the script under the restricted `student` user with a timeout of 1
    # minute, and capture the output.
    cp /data/submission/script.py /home/student/script.py
    chmod -R 700 /data
    chmod 755 /home/student/script.py
    output=$(timeout 1m sudo -u student python3 /home/student/script.py 2>&1)

    echo "[*] Python output: $output"
    if [ "$output" == "Hello World!" ]; then
        save_feedback 100 "The output is correct, well done! :)"
    else
        save_feedback 0 "The output is not correct :'("
    fi
}

handle_internal_error() {
    save_feedback 0 "An internal error occurred, please contact the teaching team."
}

# Important: make sure to always exit with 0
grade || handle_internal_error

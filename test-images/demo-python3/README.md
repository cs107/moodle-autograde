# An example image for python submission evaluation

This example shows how to create a minimal Docker image to grade a Python script.

The file submitted by the student must be named `script.py`. The grading job runs this file and captures its output. If the output is `Hello World!`, the grade is set to 100; otherwise, it is set to 0.

## Build the image 

Build the image:

```bash
docker build . -t demo-python3
```

## Local experimentation 

After building the image you can try out the grading image locally, by mounting files for grading to `/data/submission` and reading the result of `/data/feedback`. 

Example submissions are provided in `./test_submissions`. For instance, to grade the correct submission `/test_submissions/correct.py` using the image, run the following command:

```bash
rm -rf feedback
mkdir feedback
docker run --rm -v ./feedback:/data/feedback -v ./test_submissions/correct.py:/data/submission/script.py demo-python3
cat feedback/grade.json
```

You should see the following output:

```
[*] Python output: Hello World!
{"grade": 100}
```

## Autograde experimentation

To use the image as the grading base for submission, you will need to publish the image on a registry like [docker hub](https://hub.docker.com). This image is publicly available at [autograde/demo-python3](https://hub.docker.com/r/autograde/demo-python3), you can directly provide the image from this registry to the grader. 

Alternatively you can publish this image on your own registry. Following instructions are for docker hub. The first step is to tag your local image and prefix it with your registry username:
```
docker tag python_submission_example username/python_grader:latest
```

You will then have to login:
```
docker login 
````

Finally you can push the image to the registry: 
```
docker push username/python_grader:latest
```

You can provide the image you just published as the grading image for an autograded assignment.

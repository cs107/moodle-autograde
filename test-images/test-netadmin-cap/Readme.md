# Linux capabilities test

This test image is used to test the Linux capabilities of a container. It just prints the output of `capsh --print` to a feedback file named `caps.txt`, and always gives a grade of `100.0`.

Build with:

```
docker build -t test_netadmin_cap .
```

Run for example with:

```
mkdir feedback
docker run --rm --cap-add=NET_ADMIN -v ./feedback:/data/feedback test_netadmin_cap
```

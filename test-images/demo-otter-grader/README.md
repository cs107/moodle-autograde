# Autograder Jupyter notebook

The purpose of this document is to go through the steps of creating a Jupyter notebook homework graded with [other grader](https://otter-grader.readthedocs.io/en/latest/index.html), and containerzing the process for use with the Moodle Autograde plugin.

The following tutorial can be followed either with a local instance of Jupyter server or directly on the noto plateform. When encountering details options like bellow, please unfold the details for the infrastructure your are following the tutoriel on.

<details>
<summary>Local server</summary>
Unfold the local server details, to find instructions and informations for the local server.
</details>
<details>
<summary>Noto</summary>
Unfold the noto details, to find instructions and informations for Noto.
</details>

## 0. Access Jupyter Server

<details>
<summary>Local server</summary>

Build a local jupyter server, with otter-grader tools:
```
docker build . -f Dockerfile.local_server -t local_server
```

Start the server:
```
docker run \
        -it \
        -p 8888:8888 \
        -v ./jupyter_lab_workspace:/jupyter_lab_workspace \
        local_server
```

The previous command is a blocking command. It will stay in the terminal until exited with `CTRL + C`.

The command outputs some informations among which the server url after `Or copy and paste one of these URLs:`.

</details>

<details>
<summary>Noto</summary>

Access noto at [noto.epfl.ch](https://noto.epfl.ch/)
</details>

## 1. Create an assignment

The following steps are the same on the local server and on noto.

Please create a new notebook and choose the python kernel. 

The notebook will contain the assignment explanation and code placeholder, as well as the assignment configuration and tests cases.

The first cell is the assignment configuration cell, with an example of configuration below:
```
# ASSIGNMENT CONFIG
name: example
tests:
    files: false
    ok_format: false
generate: 
    pdf: false
export_cell: false
```

[!] Beware to mark the ottergrader configuration and delimiter cells as cells of type `raw cell`.

The questions are delimited with a `# BEGIN QUESTION` and a, `# END QUESTION` cell. Within those delimiters live the cell for implementation and the test cells.

The delimiter cell `# BEGIN QUESTION` also includes the question configuration:
```
# BEGIN QUESTION
name: q1
points: 2
```

Within this delimited section of the notebook, we can write 
- markdown cells for explanations.
- the cell for implementation, a code cell wrapped once again with delimiter cells: `# BEGIN SOLUTION` and `# END SOLUTION`.
- within the implementation cell we can implement the solution wrapped with `# BEGIN SOLUTION` and `# END SOLUTION`. The solutions will be replaced by ellipsis `...` when releasing the student version.
- the test cases, code cells wrapped with delimiter cells: `# BEGIN TESTS` and `# END TESTS`.

All of the delimiting raw cells and test cells are removed from the generated notebooks.

To use exception-based tests, you must set `tests: ok_format: false` in your assignment config.

You can then use assertions as tests. The test cells should define a test case function:
```python
def test_function_name(function_to_test):
    assert function_to_test(1, 1) == 2
    assert function_to_test(2, 2) == 4
```

"Any libraries or other variables declared in the student’s environment must be passed in as arguments. Otter uses the test case function arguments to pass elements from the global environment, or the global environment itself. If the test case function has an argument name env, the student’s global environment will be passed in as the value for that argument. For any other argument name, the value of that variable in the student’s global environment will be passed in; if that variable is not present, it will default to None."

Tests are public by default but can be hidden by adding the # HIDDEN comment as the first line of the cell.

You can verify the adequation of test cells with the implemented solution by calling the test function and running the notebook top-down: 
```
test_function_name(function_to_test)  # IGNORE
```

The `# IGNORE` comment removes the whole line in the released notebook.

## 2. Release the assignment

Access a terminal:
<details>
<summary>Local server</summary>

Sh in the server:
```
docker ps
docker exec -it <container_id> sh
```
</details>
<details>
<summary>Noto</summary>

Access the noto terminal, and navigate to the directory where the assignment notebook is.
</details>

Release the student version:
```
otter assign <master_notebook_name.ipynb> dist -v
```

Production:
<details>
<summary>Local server</summary>

The student version can be found in `./jupyter_lab_workspace/dist/student`.

The previous command additionaly creates a zip file, to be used as support for the autograding process. The zip can be found in `./jupyter_lab_workspace/dist/autograder`.
</details>

<details>
<summary>Noto</summary>

The student version can be found in `./dist/student`.

The previous command additionaly creates a zip file, to be used as support for the autograding process. The zip can be found in `./dist/autograder`.
</details>


## 3. Build the autograding image

Drag and drop the autograde zip from the previous step into the `./zip_file` folder on this machine. Rename the zip file `autograder.zip`.

In `./entrypoints/autograde.sh` set the `ASSIGNMENT_NAME` variable to the name of the notebook.

You can then build the autograding image:
```
docker build . -f Dockerfile.autograde -t otter_autograder
```
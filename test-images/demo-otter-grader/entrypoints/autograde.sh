#!/bin/sh
ASSIGNMENT_NAME="Untitled.ipynb" 



## grade the student notebook against the solution ziped files
echo "[+] starting to autograde..."
result=$(otter run -a /zip_file/autograder.zip "/data/submission/$ASSIGNMENT_NAME")

if [ ! -e "results.json" ]; then
    echo "[-] results file does not exists."
    echo "{\"grade\": 0}" > /data/feedback/grade.json
    exit 0
fi

search_score_line="Total Score:"
matching_line=$(echo "$result" | grep "$search_score_line")

if [[ -n "$matching_line" ]]; then
    ## extract the grade
    extracted_portion="${matching_line#*: }" 
    grade="${extracted_portion% / *}"

    ## create the grade.json file and include the results.json file as feedback
    cp results.json /data/feedback  
    echo "{\"grade\": $grade}" > /data/feedback/grade.json
    exit 0
else 
    echo "[-] problem with the grading flow"
    echo "{\"grade\": 0}" > /data/feedback/grade.json
    exit 0
fi
#!/bin/sh

cd /jupyter_lab_workspace
jupyter notebook --allow-root --ip 0.0.0.0 --port 8888 --no-browser


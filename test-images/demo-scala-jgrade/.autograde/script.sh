# IMPORTANT NOTE : EXECUTING THIS SCRIPT SHOULD ALWAYS BE 0
mkdir -p /data/feedback
mkdir -p /run_logs

# Check if the submission has the correct name/is present
echo "Unzipping the submission..."
if [ -e /data/submission/submission.zip ]; then
    unzip -q /data/submission/submission.zip -d /app/src/main/scala/
    if [ $? -eq 0 ]; then
        echo "Unzipping the submission was successful"
    else
        echo "Error: Failed to unzip the submission"
        mv /feedbacks/unzip_error.txt /data/feedback/feedback.txt
        exit 0
    fi
else
    echo "Error: submission.zip not found in /data/submission/"
    mv /feedbacks/unzip_error.txt /data/feedback/feedback.txt
    exit 0
fi

# Check that the submitted code compiles
echo "Compiling the code..."
compile_log="/run_logs/compile.log"
mvn compile > $compile_log 2>&1
if [ $? -eq 0 ]; then
  echo "Compilation successful"
  echo "------ compile.log ------"
  cat $compile_log
  echo "-------------------------"
else
  echo "Error: Compilation failed. Check '$compile_log' for details."
  echo "------ compile.log ------"
  cat $compile_log
  echo "-------------------------"
  mv /feedbacks/compilation_error.txt /data/feedback/feedback.txt
  mv $compile_log /data/feedback/compile.log
  exit 0
fi

echo "Running the tests..."
test_log="/run_logs/test.log"
timeout 5m mvn test > $test_log 2>&1 # 5 minutes timeout
if [ $? -eq 124 ]; then
  echo "Error: Tests timed out"
  echo "------ test.log ------"
  cat $test_log
  echo "-------------------------"
  mv /feedbacks/timeout.txt /data/feedback/timeout.log
  exit 0
elif [ -e "target/grade.json" ] && [ -e "target/feedback.html" ]; then
  echo "------ test.log ------"
  cat $test_log
  echo "-------------------------"
  # Move autograde related files to the correct path
  mv target/grade.json /data/feedback/grade.json
  mv target/feedback.html /data/feedback/feedback.html
else
  echo "Error: Tests failed. Autograde files not generated."
  echo "------ test.log ------"
  cat $test_log
  echo "-------------------------"
  mv /feedbacks/test_crash.txt /data/feedback/test.log
  exit 0
fi

exit 0

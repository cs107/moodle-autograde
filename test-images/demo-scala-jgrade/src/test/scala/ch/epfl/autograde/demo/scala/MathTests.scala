package ch.epfl.autograde.demo.scala

import io.github.hamzaremmal.jgrade.api.*
import io.github.hamzaremmal.jgrade.api.assertions.JGradeBasicAssertions

/**
 * JGrade Task to grade the content of [[Math]]
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.0.0
 */
@JGradeTask(name = "Implementation of Math.java", order = 1)
class MathTests()
// TODO: This is a dirty hack because JGrade only support JVM static-inner classes at the moment
//      of course this will be fixed at some point

object MathTests:

  @JGradeTask(name = "Implementation of Math::add", order = 1)
  final class AdditionTest(private val basic: JGradeBasicAssertions):
    @JGradeTest(name = "Correct behavior of Math::add", score = 1)
    def correctness(@Random a: Int, @Random b: Int): Unit =
      basic.assertEquals(a + b, Math.add(a, b))
  end AdditionTest

  @JGradeTask(name = "Implementation of Math::sub", order = 2)
  final class SubtractionTest(private val basic: JGradeBasicAssertions):
    @JGradeTest(name = "Correct behavior of Math::sub", score = 1)
    def correctness(@Random a: Int, @Random b: Int): Unit =
      basic.assertEquals(a - b, Math.sub(a, b))
  end SubtractionTest

  @JGradeTask(name = "Implementation of Math::mul", order = 3)
  final class MultiplicationTest(private val basic: JGradeBasicAssertions):
    @JGradeTest(name = "Correct behavior of Math::mul", score = 1)
    def correctness(@Random a: Int, @Random b: Int): Unit =
      basic.assertEquals(a * b, Math.mul(a, b))
  end MultiplicationTest

  @JGradeTask(name = "Implementation of Math::div", order = 4)
  final class DivisionTest(private val basic: JGradeBasicAssertions):
    @JGradeTest(name = "Correct behavior of Math::div", score = 1)
    def correctness(@Random a: Int, @Random b: Int): Unit =
      basic.assertEquals(a / b, Math.div(a, b))

    @JGradeTest(name = "Math::div throws IllegalArgumentException when dividing by 0", score = 1)
    def divByZero(@Random a: Int): Unit =
      basic.assertThrows(classOf[IllegalArgumentException], () => Math.div(a, 0))
  end DivisionTest

end MathTests

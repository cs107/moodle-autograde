package ch.epfl.autograde.demo.scala

/**
 * Simple utility class to perform simple mathematical operations
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.0.0
 */
object Math:

  /**
   * Performs the addition on integers
   *
   * @param a lhs operand
   * @param b rhs operand
   * @return the addition of both operands
   */
  def add(a: Int, b: Int) = a + b

  /**
   * Performs the subtraction on integers
   *
   * @param a lhs operand
   * @param b rhs operand
   * @return the subtraction of both operands
   */
  def sub(a: Int, b: Int) = a - b

  /**
   * Performs the multiplication on integers
   *
   * @param a lhs operand
   * @param b rhs operand
   * @return the multiplication of both operands
   */
  def mul(a: Int, b: Int) = a * b

  /**
   * Performs the division on integers
   *
   * @param a lhs operand
   * @param b rhs operand
   * @return the division of both operands
   */
  def div(a: Int, b: Int) =
    if (b == 0) throw new IllegalArgumentException
    a / b
  end div

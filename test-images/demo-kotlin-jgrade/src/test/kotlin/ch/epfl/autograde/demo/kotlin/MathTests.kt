package ch.epfl.autograde.demo.kotlin

import ch.epfl.autograde.demo.kotlin.Math.add
import ch.epfl.autograde.demo.kotlin.Math.div
import ch.epfl.autograde.demo.kotlin.Math.mul
import ch.epfl.autograde.demo.kotlin.Math.sub
import io.github.hamzaremmal.jgrade.api.JGradeTask
import io.github.hamzaremmal.jgrade.api.JGradeTest
import io.github.hamzaremmal.jgrade.api.Random
import io.github.hamzaremmal.jgrade.api.assertions.JGradeBasicAssertions

/**
 * JGrade Task to grade the content of [Math]
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.0.0
 */
@JGradeTask(name = "Implementation of Math.kotlin", order = 1)
class MathTests {

    @JGradeTask(name = "Implementation of Math::add", order = 1)
    class AdditionTest(private val basic: JGradeBasicAssertions) {
        @JGradeTest(name = "Correct behavior of Math::add", score = 1.0)
        fun correctness(@Random a: Int, @Random b: Int) {
            basic.assertEquals(a + b, add(a, b))
        }
    }

    @JGradeTask(name = "Implementation of Math::sub", order = 2)
    class SubtractionTest(private val basic: JGradeBasicAssertions) {
        @JGradeTest(name = "Correct behavior of Math::sub", score = 1.0)
        fun correctness(@Random a: Int, @Random b: Int) {
            basic.assertEquals(a - b, sub(a, b))
        }
    }

    @JGradeTask(name = "Implementation of Math::mul", order = 3)
    class MultiplicationTest(private val basic: JGradeBasicAssertions) {

        @JGradeTest(name = "Correct behavior of Math::mul", score = 1.0)
        fun correctness(@Random a: Int, @Random b: Int) {
            basic.assertEquals(a * b, mul(a, b))
        }

    }

    @JGradeTask(name = "Implementation of Math::div", order = 4)
    class DivisionTest(private val basic: JGradeBasicAssertions) {
        @JGradeTest(name = "Correct behavior of Math::div", score = 1.0)
        fun correctness(@Random a: Int, @Random b: Int) {
            basic.assertEquals(a / b, div(a, b))
        }

        @JGradeTest(name = "Math::div throws IllegalArgumentException when dividing by 0", score = 1.0)
        fun divByZero(@Random a: Int) {
            basic.assertThrows(IllegalArgumentException::class.java, { div(a, 0) })
        }
    }

}

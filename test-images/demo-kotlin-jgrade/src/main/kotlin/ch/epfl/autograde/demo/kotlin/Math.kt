package ch.epfl.autograde.demo.kotlin

/**
 * Simple utility class to perform simple mathematical operations
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.0.0
 */
object Math {

    /**
     * Performs the addition on integers
     * @param a lhs operand
     * @param b rhs operand
     * @return the addition of both operands
     */
    fun add(a: Int, b: Int): Int {
        return a + b
    }

    /**
     * Performs the subtraction on integers
     * @param a lhs operand
     * @param b rhs operand
     * @return the subtraction of both operands
     */
    fun sub(a: Int, b: Int): Int {
        return a - b
    }

    /**
     * Performs the multiplication on integers
     * @param a lhs operand
     * @param b rhs operand
     * @return the multiplication of both operands
     */
    fun mul(a: Int, b: Int): Int {
        return a * b
    }

    /**
     * Performs the division on integers
     * @param a lhs operand
     * @param b rhs operand
     * @return the division of both operands
     */
    fun div(a: Int, b: Int): Int {
        require(b != 0)
        return a / b
    }

}

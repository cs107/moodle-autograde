# Moodle Autograde

![GitLab Release](https://img.shields.io/gitlab/v/release/cs107%2Fmoodle-autograde?gitlab_url=https%3A%2F%2Fgitlab.epfl.ch&sort=semver&display_name=release&style=flat-square&label=Latest%20Release)
![GitLab License](https://img.shields.io/gitlab/license/cs107%2Fmoodle-autograde?gitlab_url=https%3A%2F%2Fgitlab.epfl.ch&style=flat-square)


This repository hosts an autograding system for Moodle. It is composed of a Moodle plugin and a web service that launches grading jobs on a Kubernetes cluster. By providing Docker images, teachers can define arbitrary grading environments for their assignments—the system is language-agnostic and supports different feedback formats.

## Documentation

- **[Overview Slides](https://docs.google.com/presentation/d/1I6cxSl5_9RdUbUu0-JtgHDPcIOuj2h5b37QV55cT9NI/edit?usp=sharing)**. These slides provide a high-level overview of the project.
- **[Practice Paper](https://doi.org/10.5281/zenodo.14256923)**. Published at SEFI 2024, the practice paper describes the project's goals and the architecture of the system in more detail.
- **[Architecture Diagram](./docs/architecture.pdf)**. Overview of the data flow between the different components of the system, with a focus on security.
- **[Example Python image](./test-images/demo-python3)**. This example shows how to create a Docker image to grade Python assignments.
- **[Administrator instructions](./docs/moodle_config.md)**. These instructions are for the Moodle administrators.
- **[Developers' instructions](./docs/dev_setup.md)**. These instructions explain how to setup a local environment to develop the Moodle plugin and the web service.

## Support 

Contact the team at [autograde-support@groupes.epfl.ch](mailto:autograde-support@groupes.epfl.ch) for any questions or issues.

## Acknowledgements

- **Main development**: [Hamza Remmal](https://remmal.net)
- **Development**: Dixit Sabharwal, [Benoit Morawiec](https://people.epfl.ch/benoit.morawiec) and [El Hassan Jamaly](https://people.epfl.ch/el.jamaly)
- **Technical supervision**: [Matt Bovel](https://people.epfl.ch/matthieu.bovel)
- **Project leads**: [Barbara Jobstmann](https://people.epfl.ch/barbara.jobstmann) and [Jamila Sam](https://people.epfl.ch/jamila.sam)

This project was funded by the [DRIL Fund](https://www.epfl.ch/education/educational-initiatives/cede/training-and-support/dril/) of EPFL.

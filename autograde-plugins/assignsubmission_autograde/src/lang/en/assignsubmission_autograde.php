<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'assignsubmission_autograde' - language : 'en'
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright 2023 AUTOGRADE-EPFL <autograde-support@groupes.epfl.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

// ============================================= GLOBAL ===========================================
$string['pluginname'] = 'Autograde';
// ======================================= ADMIN SETTINGS =========================================
$string['autograde:admin-settings:base-url'] = 'Service URL';
$string['autograde:admin-settings:base-url:description'] = 'URL to the autograde service';
$string['autograde:admin-settings:api-token'] = "API Key";
$string['autograde:admin-settings:api-token:description'] = "API Key to the autograde service" ;
// =========================================== PRIVACY ============================================
$string['privacy:metadata'] = 'The Autograde plugin only send requests to a cluster for grading. Submissions and feedback are only stored on Moodle.';
// ======================================== CAPABILITIES ==========================================
$string['autograde:viewsubmissions'] = 'Allow to download and view metadata of a submission';
$string['autograde:viewsubmission'] = 'Allow to view the metadata of a unique submission' ;
$string['autograde:grade'] = 'Allow to grade and upload the feedback of an assignment';
$string['autograde:use'] = 'Allow to use the autograde plugin';
$string['autograde:exportsubmissions'] = 'Allow to read the metadata about the submissions for an assignment';
// ===================================== ASSIGNMENT SETTINGS ======================================
$string['enabled'] = 'autograde';
$string['enabled_help'] = 'If enabled, submissions will be automatically graded after the submission';
$string['grader-image'] = 'Grader image';
$string['grader-image_help'] = 'Provide an autograde compatible image to use for grading (with the tag)';
$string['image-registry'] = 'Image registry';
$string['image-registry_help'] = 'The url to the image registry where the grading image is stored';
$string['image-registry-username'] = 'Image Registry Username';
$string['image-registry-username_help'] = 'The username to use when accessing the registry. Prefer the use of bot accounts';
$string['image-registry-token'] = 'Image Registry Password';
$string['image-registry-token_help'] = 'The password to use when accessing the registry. Prefer the use of bot accounts';
$string['env-variables'] = 'Environment variables';
$string['env-variables_help'] = 'Environment variables to be passed to the grading container. Use the format KEY=VALUE, and separate multiple variables with a semicolon.';
// ======================================= NOTIFICATIONS ==========================================
$string['autograde:http-0']   = 'Autograde service not found. Please contact the support.';
$string['autograde:http-404'] = "The call to the autograde api resulted in a NOT-FOUND. An existing service is running on this server but it's not autograde. Verify that the autograde service URL is correct.";
$string['autograde-assignment:create-successful'] = 'Successfully created the assignment in the autograde service.';
$string['autograde-assignment:delete-successful'] = 'Successfully deleted the assignment from the autograde service.';
$string['autograde-submission:create-successful'] = 'Your submission was successfully uploaded. Your grade will be available upon completion of the grading process.';
$string['settings-autograde:http-200'] = 'The credentials were correctly updated in the service.';
$string['settings-autograde:http-403'] = 'The provided autograde access token is not valid.';
$string['submit-autograde:http-403'] = 'Your submission was not scheduled for grading. Please contact your teacher or one of the TAs to fix the issue.';
// ========================================== ERRORS ==============================================
$string['autograde_base_url_not_configured'] = 'The autograde service URL is not configured. Please contact the admins to fix the issue.';

<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'assignsubmission_autograde' - language : 'fr'
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright 2023 AUTOGRADE-EPFL <autograde-support@groupes.epfl.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

// ============================================= GLOBAL ===========================================
$string['pluginname'] = 'Autograde';
// ======================================= ADMIN SETTINGS =========================================
$string['autograde:admin-settings:base-url'] = 'URL du service';
$string['autograde:admin-settings:base-url:description'] = "URL pour accéder à l'API de autograde" ;
$string['autograde:admin-settings:api-token'] = "Clé d'accès au service";
$string['autograde:admin-settings:api-token:description'] = "Clé pour accéder à l'API de autograde" ;
// =========================================== PRIVACY ============================================
$string['privacy:metadata'] = "Le plugin Autograde envoie uniquement des requêtes à un cluster pour l'évaluation. Les soumissions et les commentaires ne sont stockés que sur Moodle.";
// ======================================== CAPABILITIES ==========================================
$string['autograde:viewsubmissions'] = "Autoriser le téléchargement d'une soumission compatible avec Autograde";
$string['autograde:viewsubmission'] = "Autoriser la visualisation d'une soumission unique";
$string['autograde:grade'] = "Autoriser le téléchargement du feedback d'une soumission compatible avec Autograde";
$string['autograde:use'] = 'Accès au service autograde';
$string['autograde:exportsubmissions'] = "Autoriser la lecture des métadonnées d'un devoir";
// ===================================== ASSIGNMENT SETTINGS ======================================
$string['enabled'] = 'autograde';
$string['enabled_help'] = 'Si cette option est activée, les soumissions seront automatiquement notées après la soumission';
$string['grader-image'] = 'Image du correcteur';
$string['grader-image_help'] = 'Fournissez une image compatible avec la notation automatique à utiliser pour la notation (avec la balise)';
$string['image-registry'] = "Registre d'image";
$string['image-registry_help'] = "L'URL du registre d'images où l'image d'évaluation est stockée";
$string['image-registry-username'] = "Nom d'utilisateur";
$string['image-registry-username_help'] = "Le nom d'utilisateur à utiliser lors de l'accès au registre. Préférer l'utilisation de comptes de bots";
$string['image-registry-token'] = 'Mot de passe';
$string['image-registry-token_help'] = "Le mot de passe à utiliser pour accéder au registre. Préférer l'utilisation de comptes de bots";
$string['env-variables'] = 'Variables d\'environnement';
$string['env-variables_help'] = "Variables d'environnement à transmettre au container du correcteur. Utiliser le format CLÉ=VALEUR, et séparer les variables par des point-virgules.";
// ======================================= NOTIFICATIONS ==========================================
$string['autograde:http-0']   = "Impossible d'établir une communication avec le service autograde. Merci de contacter le support.";
$string['settings-autograde:http-200'] = "Les informations d'identification ont été mis à jour.";
$string['autograde-assignment:create-successful'] = 'Devoir créé avec succès dans le service autograde.';
$string['autograde-assignment:delete-successful'] = 'Devoir supprimé avec succès du service autograde.';
$string['autograde-submission:create-successful'] = "Votre devoir a été correctement remis. Votre note sera disponible une fois le processus d'évaluation terminé.";
$string['settings-autograde:http-403'] = "Le jeton d'accès autograde n'est pas valide.";
$string['submit-autograde:http-403'] = "Votre soumission n'a pas été ajoutés à la queue pour correction. Merci de contacter votre professeur ou TA.";
// ========================================== ERRORS ==============================================
$string['autograde_base_url_not_configured'] = "L'URL du service autograde n'est pas configurée. Merci de contacter les admins pour corriger l'erreur.";

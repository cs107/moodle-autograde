<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * This file defines the admin settings for this plugin
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright 2023 AUTOGRADE-EPFL <autograde-support@groupes.epfl.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once(__DIR__ . '/lib.php');

$settings->add(
    new admin_setting_configtext(
        "assignsubmission_autograde/service_url",
        get_string('autograde:admin-settings:base-url', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME),
        get_string('autograde:admin-settings:base-url:description', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME),
        "",
        PARAM_URL)
);

$settings->add(
    new admin_setting_encryptedpassword(
        'assignsubmission_autograde/api_token',
        get_string('autograde:admin-settings:api-token', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME),
        get_string('autograde:admin-settings:api-token:description', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME)
    )
);
<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * This file defines utility functions
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright 2023 AUTOGRADE-EPFL <autograde-support@groupes.epfl.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace assignsubmission_autograde\utils;

use assign;
use context_module;
use dml_exception;
use moodle_exception;

/**
 *
 * @return assign
 * @throws moodle_exception
 * @throws dml_exception
 */
function assignsubmission_autograde_get_assignment(int $id) : assign {
    global $DB;
    // HR : Fetch the information from the metadata
    $assign = $DB->get_record('assign', ['id' => $id], 'id', MUST_EXIST);
    // HR : Fetch the course and the course module instances
    list($course, $cm) = get_course_and_cm_from_instance($assign, 'assign');
    // HR : Build the context
    $context = context_module::instance($cm->id);
    // HR : Build the assignment and return it
    return new assign($context, $cm, $course);
}

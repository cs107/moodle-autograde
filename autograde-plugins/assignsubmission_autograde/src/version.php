<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * This file contains the plugin informations about the autograde plugin
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright 2023 AUTOGRADE-EPFL <autograde-support@groupes.epfl.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2024102900; // HR : October 29th, 2024
$plugin->requires  = 2022112800; // HR : Moodle 4.1.0 (https://moodledev.io/general/releases#moodle-41-lts)
$plugin->maturity = MATURITY_RC;
$plugin->release = '1.2.2';
$plugin->component = 'assignsubmission_autograde';
$plugin->dependencies = array(
    'mod_assign' => '2022112800',
    'webservice_rest' => '2022112800',
    'assignsubmission_file' => '2022112800',
    'assignfeedback_file' => '2022112800',
);

<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * This file provides common constants that are used in different files
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright 2023 AUTOGRADE-EPFL <autograde-support@groupes.epfl.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/** ??? */
const ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME = 'assignsubmission_autograde';

/**
 * ???
 * @param $user
 * @return stdClass
 */
function assignsubmission_autograde_createuser($user): stdClass {
    $_user = new stdClass();
    $_user->id = $user->id;
    $_user->username = $user->username;
    $_user->firstname = $user->firstname;
    $_user->lastname = $user->lastname;
    $_user->idnumber = $user->idnumber;
    $_user->email = $user->email;
    return $_user;
}

function assignsubmission_autograde_submitters($submission): array {
    global $DB;
    $result = array();
    if($submission->userid != 0){
        $user = $DB->get_record( 'user', [ 'id' => $submission->userid ]);
        $_user = assignsubmission_autograde_createuser($user);
        $result[] = $_user;
    } else {
        $groupmembers = groups_get_members($submission->groupid,'u.*');
        foreach ($groupmembers as $member){
            $_user = assignsubmission_autograde_createuser($member);
            $result[] = $_user ;
        }
    }
    return $result;
}
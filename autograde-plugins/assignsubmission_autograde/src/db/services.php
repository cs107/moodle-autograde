<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * This file defines the moodle service and functions used by autograde
 *
 * @see https://moodledev.io/docs/apis/subsystems/external/description
 * @see https://moodledev.io/docs/apis/subsystems/external/advanced/custom-services
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright 2023 AUTOGRADE-EPFL <autograde-support@groupes.epfl.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$functions = [
   'mod_assignsubmission_autograde_upload_feedback' => [
       'classname' => 'assignsubmission_autograde\external\autograde_upload_feedback',
       'methodname' => 'upload_feedback',
       'description' => 'Upload an autograde feedback',
       'capabilities' => 'assignsubmission/autograde:grade',
   ],
    'mod_assignsubmission_autograde_download_submission' => [
        'classname' => 'assignsubmission_autograde\external\autograde_download_submission',
        'methodname' => 'download_submission',
        'description' => 'Download an autograde submission',
        'capabilities' => 'assignsubmission/autograde:viewsubmissions',
    ],
    'mod_assignsubmission_autograde_list_submissions' => [
      'classname'    => 'assignsubmission_autograde\external\autograde_list_submissions' ,
      'methodname'   => 'list_submissions',
      'description'  => 'List all of the submissions for a given assignment',
      'capabilities' => 'assignsubmission/autograde:exportsubmissions'
    ],
    'mod_assignsubmission_autograde_submission_info' => [
        'classname'    => 'assignsubmission_autograde\external\autograde_submission_info' ,
        'methodname'   => 'info_submissions',
        'description'  => 'List all of the information for a given submission',
        'capabilities' => 'assignsubmission/autograde:viewsubmission'
    ],
    'mod_assignsubmission_autograde_download_all_submissions' => [
    'classname'    => 'assignsubmission_autograde\external\autograde_download_all_submissions' ,
    'methodname'   => 'download_all_submissions',
    'description'  => 'Download all submissions of a given assignment',
]
];

$services = [
  'AUTOGRADE' => [
      'functions' => [
          'mod_assignsubmission_autograde_upload_feedback',
          'mod_assignsubmission_autograde_download_submission',
          'mod_assignsubmission_autograde_list_submissions',
          'mod_assignsubmission_autograde_submission_info',
          'mod_assignsubmission_autograde_download_all_submissions',
      ],
      'restrictedusers' => 1,
      'enabled' => 1,
      'shortname' => 'autograde-service'
  ]
];

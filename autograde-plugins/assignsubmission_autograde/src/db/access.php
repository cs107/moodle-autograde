<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * This file defines the capabilities used autograde
 *
 * @see https://docs.moodle.org/dev/NEWMODULE_Adding_capabilities
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright 2023 AUTOGRADE-EPFL <autograde-support@groupes.epfl.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$capabilities = array(
    // HR : Capability to read an autograde submission
    'assignsubmission/autograde:viewsubmissions' => array(
        'riskbitmask' => RISK_PERSONAL,
        'captype' => 'read',
        'contextlevel' => CONTEXT_COURSE,
        'archetypes' => array(),
        'clonepermissionsfrom' => 'mod/assign/submission/autograde:read_submission'

    ),
    // HR : Capability to write a feedback
    'assignsubmission/autograde:grade' => array(
        'riskbitmask' => RISK_SPAM | RISK_XSS,
        'captype' => 'write',
        'contextlevel' => CONTEXT_COURSE,
        'archetypes' => array(),
        'clonepermissionsfrom' => 'mod/assign/submission/autograde:update_feedback'
    ),
    // HR : Capability to use the service
    'assignsubmission/autograde:use' => array(
        'captype' => 'read',
        'contextlevel' => CONTEXT_COURSE,
        'archetypes' => array(),
        'clonepermissionsfrom' => 'mod/assign/submission/autograde:use_service'
    ),
    // HR : Capability to read the submissions
    'assignsubmission/autograde:exportsubmissions' => array(
        'captype' => 'read',
        'contextlevel' => CONTEXT_COURSE,
        'archetypes' => array(),
        'clonepermissionsfrom' => 'mod/assign/submission/autograde:list_submissions'
    ),
    'assignsubmission/autograde:viewsubmission' => array(
        'captype' => 'read',
        'contextlevel' => CONTEXT_COURSE,
        'archetypes' => array()
    )
);

$deprecatedcapabilities = array(
    'mod/assign/submission/autograde:read_submission' => [
        'replacement' => 'assignsubmission/autograde:viewsubmissions',
        'message' => 'This capability name was changed to follow the pattern'
    ],
    'mod/assign/submission/autograde:update_feedback' => [
        'replacement' => 'assignsubmission/autograde:grade',
        'message' => 'This capability name was changed to follow the pattern'
    ],
    'mod/assign/submission/autograde:use_service' => [
        'replacement' => 'assignsubmission/autograde:use',
        'message' => 'This capability name was changed to follow the pattern'
    ],
    'mod/assign/submission/autograde:list_submissions' => [
        'replacement' => 'assignsubmission/autograde:exportsubmissions',
        'message' => 'This capability name was changed to follow the pattern'
    ],
);
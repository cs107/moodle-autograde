<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * This file provides the definition of the autograde submission plugin
 *
 * This plugin serves submissions to a webserver, so they can be graded automatically.
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright 2023 AUTOGRADE-EPFL <autograde-support@groupes.epfl.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

use assignsubmission_autograde\client\model\Assignment;
use assignsubmission_autograde\client\model\Environment;
use assignsubmission_autograde\client\model\Registry;
use assignsubmission_autograde\client\model\RegistryCredentials;
use assignsubmission_autograde\client\model\Submission;
use assignsubmission_autograde\client\model\User;
use assignsubmission_autograde\client\request\CreateAssignmentRequest;
use assignsubmission_autograde\client\request\CreateSubmissionRequest;
use assignsubmission_autograde\client\autograde_webservice;
use assignsubmission_autograde\client\model\Course;
use core\notification;

global $CFG;

require($CFG->libdir.'/form/password.php');
require_once(__DIR__.'/lib.php');
require_once(__DIR__.'/utils.php');

/**
 * Definition of the autograde submission plugin
 *
 * This plugin serves submissions to a webserver, so they can be graded automatically.
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright ???
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
final class assign_submission_autograde extends assign_submission_plugin
{

    /**
     * Get the name of this plugin
     *
     * @note See documentation : https://moodledev.io/docs/apis/plugintypes/assign/submission#get_name
     * @return string plugin name
     * @throws coding_exception
     */
    public function get_name(): string
    {
        return get_string("pluginname", ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
    }

    /**
     * ???
     * @throws coding_exception ???
     */
    public function is_configurable(): bool
    {
        // HR : Allow configuring autograde if one of the conditions is satisfied :
        // HR :     - previously enabled in the assignment
        // HR :     - A user with the 'assignsubmission/autograde:use' is enrolled in the course
        return $this->is_enabled() ||
            !empty(get_users_by_capability(
                context_course::instance($this->assignment->get_course()->id),
                'assignsubmission/autograde:use'
            )
            );
    }

    // ============================================================================================
    // ======================================= SETTINGS ===========================================
    // ============================================================================================

    /**
     * Configure the settings for the docker_submission plugin
     *
     * We only need the docker image of the grader to run
     * @note See documentation : https://docs.moodle.org/dev/Form_API
     * @note See documentation : https://docs.moodle.org/dev/lib/formslib.php_Form_Definition
     * @param MoodleQuickForm $mform
     * @return void
     * @throws coding_exception
     */
    public function get_settings(MoodleQuickForm $mform)
    {
        $image = new MoodleQuickForm_text('grader-image', get_string('grader-image', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME), null);
        $registry = new MoodleQuickForm_text('image-registry', get_string('image-registry', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME), null);
        $username = new MoodleQuickForm_text('image-registry-username', get_string('image-registry-username', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME), null);
        $token = new MoodleQuickForm_password('image-registry-token', get_string('image-registry-token', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME), null);
        $env_variables = new MoodleQuickForm_text('env-variables', get_string('env-variables', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME), null);
        $mform->addElement($image);
        $mform->addElement($registry);
        $mform->addElement($username);
        $mform->addElement($token);
        $mform->addElement($env_variables);
        $mform->hideIf($image->getName(), 'assignsubmission_autograde_enabled');
        $mform->hideIf($registry->getName(), 'assignsubmission_autograde_enabled');
        $mform->hideIf($username->getName(), 'assignsubmission_autograde_enabled');
        $mform->hideIf($token->getName(), 'assignsubmission_autograde_enabled');
        $mform->hideIf($env_variables->getName(), 'assignsubmission_autograde_enabled');
        $mform->addHelpButton($image->getName(), 'grader-image', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
        $mform->addHelpButton($registry->getName(), 'image-registry', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
        $mform->addHelpButton($username->getName(), 'image-registry-username', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
        $mform->addHelpButton($token->getName(), 'image-registry-token', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
        $mform->addHelpButton($env_variables->getName(), 'env-variables', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
    }

    /**
     * ???
     * @param $defaultvalues ???
     */
    public function data_preprocessing(&$defaultvalues)
    {
        parent::data_preprocessing($defaultvalues);
        // HR : If the assignment is already created, fetch the old configuration
        if ($key = $this->get_config('grader-image'))
            $defaultvalues['grader-image'] = $key;
        if ($key = $this->get_config('image-registry'))
            $defaultvalues['image-registry'] = $key;
        if ($key = $this->get_config('image-registry-username'))
            $defaultvalues['image-registry-username'] = $key;
        if ($key = $this->get_config('image-registry-token'))
            $defaultvalues['image-registry-token'] = $key;
        if ($key = $this->get_config('env-variables'))
            $defaultvalues['env-variables'] = $key;
    }

    /**
     * Save the settings of the docker_submission plugin
     *
     * @note See documentation : https://moodledev.io/docs/apis/plugintypes/assign/submission#save_settings
     * @param stdClass $data ???
     * @return bool ???
     */
    public function save_settings(stdClass $data): bool {
        // HR : Store the Docker image in the configuration
        $this->set_config('grader-image', $data->{'grader-image'});
        $this->set_config('image-registry', $data->{'image-registry'});
        $this->set_config('image-registry-username', $data->{'image-registry-username'});
        $this->set_config('image-registry-token', $data->{'image-registry-token'});
        $this->set_config('env-variables', $data->{'env-variables'});

        try {
            $assignment = $this->assignment->get_instance();
            $course = $this->assignment->get_course();
            $request =
                new CreateAssignmentRequest(
                    new Assignment($assignment->id),
                    new Course($course->id, $course->shortname, $course->fullname),
                    new Registry(
                        $data->{'image-registry'},
                        new RegistryCredentials(
                            $data->{'image-registry-username'},
                            $data->{'image-registry-token'}
                        )
                    )
                );
            $response = (new autograde_webservice())->create_assignment($request);
            notification::info($response);
            return true;
        } catch (moodle_exception $moodle_exception) {
            $this->set_error($moodle_exception->getMessage());
            return false;
        }
    }

    // ============================================================================================
    // ================================= ASSIGNMENT MANAGEMENT ====================================
    // ============================================================================================

    /**
     * Callback to execute when the assignment is deleted
     * @return bool ???
     * @throws moodle_exception
     */
    public function delete_instance(): bool {
        if ($this->is_enabled()) {
            $client = new autograde_webservice();
            $client->delete_assignment($this->assignment->get_instance()->id);
        }
        return true;
    }

    /**
     * Callback to check is the assignment has any submission
     *
     * @note See documentation : https://moodledev.io/docs/apis/plugintypes/assign/submission#is_empty
     * @param stdClass $submissionorgrade
     * @return bool
     */
    public function is_empty(stdClass $submissionorgrade): bool {
        return true;
    }


    // ============================================================================================
    // ================================== SUBMISSION FORM =========================================
    // ============================================================================================

    /**
     * ???
     *
     * @note See documentation : https://moodledev.io/docs/apis/plugintypes/assign/submission#get_form_elements
     * @param $submissionorgrade
     * @param MoodleQuickForm $mform
     * @param stdClass $data
     * @param $userid
     * @return bool
     */
    public function get_form_elements_for_user($submissionorgrade,
                                               MoodleQuickForm $mform,
                                               stdClass $data,
                                               $userid): bool {
        return false;
    }

    // ============================================================================================
    // ================================= SUBMISSION MANAGEMENT ====================================
    // ============================================================================================

    /**
     * ???
     */
    public function save(stdClass $submissionorgrade, stdClass $data): bool {
        return true;
    }

    /**
     * Callback when a submission is being deleted
     * @param stdClass $submission ???
     */
    public function remove(stdClass $submission){
        // HR : We don't store any specific data for a submission to delete
    }

    /**
     * Callback to trigger the grading process on the cluster when the work is submitted for grading
     * @param $submission ???
     */
    public function submit_for_grading($submission){
        global $DB;
        try {
            // HR : Build the webservice API
            $autograde_webservice = new autograde_webservice();

            $assignment = $this->assignment->get_instance();
            $course     = $this->assignment->get_course();
            $user = $DB->get_record('user', array('id' => $submission->userid));

            $request = new CreateSubmissionRequest(
                new Submission($submission->id, $submission->attemptnumber),
                new Assignment($assignment->id),
                assignsubmission_autograde_submitters($submission),
                new Course($course->id, $course->shortname, $course->fullname),
                new Environment(
                    $this->get_config('grader-image'),
                    $this->parse_env_variable($this->get_config('env-variables'))
                )
            );

            // HR : Request from autograde to grade the submission
            $notification = $autograde_webservice->create_submission($request);
            notification::success($notification);
        } catch (moodle_exception $moodle_exception){
            notification::error($moodle_exception->getMessage());
        }
    }

    /**
     * Check if a submission is empty
     * @param stdClass $data
     * @return false
     */
    public function submission_is_empty(stdClass $data): bool {
        return false;
    }

    // ============================================================================================
    // ================================ SUMMARY MANAGEMENT ========================================
    // ============================================================================================

    /**
     * Should Moodle add a row in the summary page and a column in the grading table
     * @return bool true to include in the summary, false otherwise
     */
    public function has_user_summary(): bool{
        return false;
    }

    /**
     * Content of the column in the view summary or the grading_table
     *
     * @note See documentation : https://moodledev.io/docs/apis/plugintypes/assign/submission#view_summary
     * @param $submission ???
     * @param bool &$showviewlink ???
     * @return string ???
     */
    public function view_summary($submission, &$showviewlink): string {
        return '';
    }

    // ============================================================================================
    // ========================================= VIEW =============================================
    // ============================================================================================

    /**
     * HTML content to be displayed right below the
     * activity's "intro" section on the main assignment page.
     * @return string
     */
    public function view_header(): string {
        return '';
    }

    /**
     * ???
     * @param stdClass $submission
     * @return string ???
     */
    public function view(stdClass $submission): string {
        return '';
    }

    // ============================================================================================
    // ==================================== FILE MANAGEMENT =======================================
    // ============================================================================================

    /**
     * ???
     *
     * @note See documentation : https://moodledev.io/docs/apis/plugintypes/assign/submission#get_files
     * @param stdClass $submissionorgrade
     * @param stdClass $user
     * @return array
     */
    public function get_files(stdClass $submissionorgrade, stdClass $user): array {
        return array();
    }

    /**
     * ???
     * @return string[] ???
     */
    public function get_file_areas(): array {
        return array();
    }

    // ============================================================================================
    // ==================================== EXTERNAL API ==========================================
    // ============================================================================================

    /**
     * List of external parameters to be included in the definition of a webservice
     * (save_submission or save_grade for example)
     * @return external_description|null
     */
    public function get_external_parameters(): ?external_description {
        return null;
    }

    // ============================================================================================
    // ========================================== UTILS ===========================================
    // ============================================================================================

    private function parse_env_variable(string $variables): stdClass {
        $result = new stdClass();
        if (empty($variables)) return $result;
        foreach (explode(';', $variables) as $var) {
            $split = explode('=', $var);
            $result->{$split[0]} = $split[1];

        }
        return $result;
    }

}

<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * This file defines an interface to communicate with our grading service
 *
 * To grade the submissions, we communicate with our cluster through a service.
 * When requesting a new grading, our cluster will create a job and run the provided grader.
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright 2023 AUTOGRADE-EPFL <autograde-support@groupes.epfl.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace assignsubmission_autograde\client;

defined('MOODLE_INTERNAL') || die();

use assignsubmission_autograde\client\request\CreateAssignmentRequest;
use assignsubmission_autograde\client\request\CreateSubmissionRequest;
use coding_exception;
use core\encryption;
use dml_exception;
use moodle_exception;

require_once(__DIR__.'/../../lib.php');

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright ???
 * @license ???
 */
final class autograde_webservice {

    private string $base_url;

    private string $api_key;

    /**
     * @throws dml_exception
     * @throws moodle_exception
     */
    public function __construct() {
        $this->base_url = get_config(ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME, 'service_url');
        $this->api_key = encryption::decrypt(get_config(ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME, 'api_token'));
        if (empty($this->base_url)) {
            throw new moodle_exception('autograde_base_url_not_configured',
                ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
        }
    }

    /**
     * ???
     * @param int $timeout ???
     * @param bool $auth ???
     * @return mixed ???
     * @throws dml_exception ???
     */
    public function ping(int $timeout = 2, bool $auth = false) {
        // HR : Build the URL fot the grade function
        $url = $auth
            ? $this->url('/api/v1/ping/auth')
            : $this->url('/api/v1/ping/no-auth');
        // HR : Prepare the headers
        $headers = [
            $this->api_key_header($this->api_key)
        ];
        // HR : Send request using cURL
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        // HR : Only add the key in auth mode
        if ($auth)
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_exec($curl);
        curl_close($curl);
        // Return the response
        return curl_getinfo($curl);
    }

    /**
     * ???
     * @param CreateSubmissionRequest $request
     * @return string ???
     * @throws coding_exception
     * @throws dml_exception ???
     * @throws moodle_exception
     */
    public function create_submission(CreateSubmissionRequest $request) : string {
        // HR : Build the URL for the grade function
        $url = $this->url('/api/v1/submission');
        // HR : Prepare the headers
        $headers = [
            $this->api_key_header($this->api_key),
            'Content-Type: application/json'
        ];
        // HR : Prepare cURL options
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($request),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_CONNECTTIMEOUT_MS => 10000,
            CURLOPT_TIMEOUT_MS => 10000
        );
        // HR : Send request using cURL
        $curl = curl_init();
        curl_setopt_array($curl, $options);
        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);
        // Return the response
        switch ($info['http_code']) {
            case 0   :
                throw new moodle_exception('autograde:http-0', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
            case 201 :
                return get_string('autograde-submission:create-successful', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
            case 403 :
                throw new moodle_exception('submit-autograde:http-403', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
            case 404 :
                throw new moodle_exception('autograde:http-404', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
            default  :
                return $info['http_code'] . ' -> ' . $response;
        }
    }

    // ============================================================================================
    // ==================== ASSIGNMENT: /api/v1/assignment/** =====================================
    // ============================================================================================

    /**
     * ???
     * @param CreateAssignmentRequest $request ???
     * @return string ???
     * @throws dml_exception
     * @throws moodle_exception
     */
    public function create_assignment(CreateAssignmentRequest $request): string
    {
        // HR : Build the URL for the grade function
        $url = $this->url('/api/v1/assignment');
        // HR : Prepare the headers
        $headers = [
            $this->api_key_header($this->api_key),
            'Content-Type: application/json'
        ];
        // HR : Prepare cURL options
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($request),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_CONNECTTIMEOUT_MS => 10000,
            CURLOPT_TIMEOUT_MS => 10000
        );
        // HR : Send request using cURL
        $curl = curl_init();
        curl_setopt_array($curl, $options);
        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);
        switch ($info['http_code']) {
            case 0   :
                throw new moodle_exception('autograde:http-0', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
            case 201 :
                return get_string('autograde-assignment:create-successful', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
            case 403 :
                throw new moodle_exception('settings-autograde:http-403', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
            case 404 :
                throw new moodle_exception('autograde:http-404', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
            default  :
                return $info['http_code'] . ' -> ' . $response;
        }
    }

    public function delete_assignment(int $id): string {
        // HR : Build the URL for the grade function
        $url = $this->url('/api/v1/assignment/'.$id);
        // HR : Prepare the headers
        $headers = [
            $this->api_key_header($this->api_key),
        ];
        // HR : Prepare cURL options
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_CONNECTTIMEOUT_MS => 10000,
            CURLOPT_TIMEOUT_MS => 10000
        );
        // HR : Send request using cURL
        $curl = curl_init();
        curl_setopt_array($curl, $options);
        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);
        switch ($info['http_code']) {
            case 0   :
                throw new moodle_exception('autograde:http-0', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
            case 204 :
                return get_string('autograde-assignment:delete-successful', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
            case 403 :
                throw new moodle_exception('settings-autograde:http-403', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
            case 404 :
                throw new moodle_exception('autograde:http-404', ASSIGNSUBMISSION_AUTOGRADE_COMPONENT_NAME);
            default  :
                return $info['http_code'] . ' -> ' . $response;
        }
    }

    // ============================================================================================
    // =================================== HELPER METHODS =========================================
    // ============================================================================================

    /**
     * ???
     * @param string $endpoint ???
     * @return string ???
     */
    private function url(string $endpoint): string {
        return $this->base_url . $endpoint;
    }

    /**
     * ???
     * @param string $key ???
     * @return string ???
     */
    private function api_key_header(string $key) : string {
        return 'API-KEY: ' . $key;
    }

}
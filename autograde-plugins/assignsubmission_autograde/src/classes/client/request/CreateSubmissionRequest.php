<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * This file defines a Data Transfer Object to create a submission
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright 2023 AUTOGRADE-EPFL <autograde-support@groupes.epfl.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace assignsubmission_autograde\client\request;

use assignsubmission_autograde\client\model\Assignment;
use assignsubmission_autograde\client\model\Course;
use assignsubmission_autograde\client\model\Environment;
use assignsubmission_autograde\client\model\Submission;
use assignsubmission_autograde\client\model\User;

defined('MOODLE_INTERNAL') || die();

/**
 * Data Transfer Object used to upload the configuration for a submission to
 * the autograde service (`/api/v1/submission/create`).
 *
 * @author Hamza REMMAL(hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright ???
 * @license ???
 */
final class CreateSubmissionRequest {

    public Submission $submission;

    public Assignment $assignment;

    public array $users;

    public Course $course;

    public Environment $environment;

    public function __construct(Submission $submission, Assignment $assignment, array $users, Course $course, Environment $environment){
        $this->submission  = $submission;
        $this->assignment  = $assignment;
        $this->users        = $users;
        $this->course      = $course;
        $this->environment = $environment;
    }

}

<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.
/**
 * This file defines the function to download all submissions
 *
 * @see https://moodledev.io/docs/apis/subsystems/external/functions
 * @author ???
 * @package ???
 * @copyright 2023 AUTOGRADE-EPFL <autograde-support@groupes.epfl.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace assignsubmission_autograde\external;

use context_course;
use external_api;
use external_function_parameters;
use external_value;
use moodle_exception;
use stdClass;
use function assignsubmission_autograde\utils\assignsubmission_autograde_get_assignment;

require_once(__DIR__.'/../../lib.php');
require_once(__DIR__.'/../../utils.php');
require_once($CFG->libdir . '/externallib.php');
require_once($CFG->dirroot . '/mod/assign/locallib.php');

class autograde_download_all_submissions extends external_api{

    /**
     * ???
     * @return external_function_parameters ???
     */
    public static function download_all_submissions_parameters(): external_function_parameters
    {
        return new external_function_parameters([
            'assignmentid' => new external_value(PARAM_INT, 'The ID of the assignment'),
        ]);
    }

    /**
     * @throws \dml_exception
     * @throws \required_capability_exception
     * @throws \moodle_exception
     */
    public static function download_all_submissions($assignmentid){
        global $DB, $USER;

        $course_id = $DB->get_field('assign', 'course', array('id' => $assignmentid));

        // HR : Check if the user has the necessary capabilities
        require_capability('assignsubmission/autograde:exportsubmissions',
            context_course::instance($course_id), $USER->id, false);

        // HR : Fetch the correct assignment instance
        $assignment = assignsubmission_autograde_get_assignment($assignmentid);

        // Check if group submissions are enabled
        $is_group_submission = $DB->get_field('assign', 'teamsubmission', ['id' => $assignmentid]);

        //Fetch the submissions from the DB
        $submissions = $is_group_submission ?
            $DB->get_records_select(
                'assign_submission',
                'assignment = :assignmentid AND groupid != 0',
                ['assignmentid' => $assignmentid]
            ):
            $DB->get_records('assign_submission', ['assignment' => $assignmentid]) ;


        if (empty($submissions)) {
            throw new moodle_exception('nosubmissions', 'mod_assign');
        }


        $files_to_zip = [];
        foreach ($submissions as $submission){
            $group_name = '';
            if ($is_group_submission) {
                // Handle group submissions
                // Fetch group members
                $group_members = $DB->get_records('groups_members', ['groupid' => $submission->groupid], '', 'userid');
                if (empty($group_members)) {
                    throw new moodle_exception('invalidgroup', 'mod_assign', '', 'Group has no members.');
                }

                // Concatenate idnumbers of group members to create the group name
                $group_idnumbers = [];
                foreach ($group_members as $member) {
                    $user = $DB->get_record('user', ['id' => $member->userid], 'idnumber', MUST_EXIST);
                    $group_idnumbers[] = $user->idnumber;
                }
                $group_name = implode('_', $group_idnumbers); // Example: "idnumber1_idnumber2"
            } else {
                // Handle individual submissions
                if (!empty($submission->userid)) {
                    $user = $DB->get_record('user', ['id' => $submission->userid], 'idnumber', MUST_EXIST);
                    $group_name = $user->idnumber;
                } else {
                    throw new moodle_exception('invalidsubmission', 'mod_assign', '', 'Submission has no valid user or group ID.');
                }
            }

            // HR : Retrieve the files associated with the specified area and item
            $submission_files = array();
            foreach ($assignment->get_submission_plugins() as $plugin){
                if($plugin->is_enabled() && $plugin->is_visible() && $plugin->allow_submissions())
                    $submission_files += $plugin->get_files($submission, new stdClass());
            }


            foreach ($submission_files as $file) {
                $filepath = $file->get_filepath();
                $filename = $file->get_filename();
                // We observed by looking in the $DB that the $filepath always starts and ends with
                // '/'. If the filepath is empty, then it's the single character '/'
                $files_to_zip[$group_name . '/' . $submission->attemptnumber . $filepath . $filename] = $file;
            }

        }

        if (empty($files_to_zip)) {
            throw new moodle_exception('nofiles', 'mod_assign');
        }

        //Prepare a temporary file for zipping
        $tempdir = make_temp_directory('submission_files') ;
        $zipfilename = "assignment_{$assignmentid}_submissions.zip" ;
        $tempzipfile = "$tempdir/$zipfilename" ;

        //Use Moodle's packer to create a zip file
        $packer = get_file_packer('application/zip') ;

        //Pack the files into the zip archive
        if (!$packer->archive_to_pathname($files_to_zip, $tempzipfile)) {
            throw new moodle_exception('couldnotcreatezip');
        }

        //Send the zip file as a response using Moodle's API
        send_temp_file($tempzipfile, $zipfilename);



    }

    /**
     * @return void
     */
    public static function download_all_submissions_returns() {
    }
}
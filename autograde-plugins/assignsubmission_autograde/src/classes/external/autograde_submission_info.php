<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * This file defines the function to have informations about a submission
 *
 * @see https://moodledev.io/docs/apis/subsystems/external/functions
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright 2023 AUTOGRADE-EPFL <autograde-support@groupes.epfl.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace assignsubmission_autograde\external;

defined('MOODLE_INTERNAL') || die();

use assignsubmission_autograde\client\model\Submission;
use coding_exception;
use context_course;
use external_api;
use external_function_parameters;
use external_multiple_structure;
use external_single_structure;
use external_value;
use moodle_exception;
use stdClass;
use function assignsubmission_autograde\utils\assignsubmission_autograde_get_assignment;

global $CFG;

require_once(__DIR__.'/../../lib.php');
require_once(__DIR__.'/../../utils.php');
require_once($CFG->libdir . '/externallib.php');
require_once($CFG->dirroot . '/mod/assign/locallib.php');

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright ???
 * @license ???
 */
final class autograde_submission_info extends external_api {

    /**
     * ???
     * @return external_function_parameters ???
     */
    public static function info_submissions_parameters(): external_function_parameters
    {
        return new external_function_parameters([
            'sid' => new external_value(PARAM_INT, 'The ID of the submission'),
        ]);
    }

    /**
     * ???
     * @param $assignmentid ???
     * @return stdClass[] ???
     * @throws coding_exception ???
     * @throws moodle_exception ???
     */
    public static function info_submissions($sid): stdClass {
        global $DB, $USER;
        self::validate_parameters(self::info_submissions_parameters(), ['sid' => $sid]);

        $submission = $DB->get_record('assign_submission', ['id' => $sid ]);
        $assignment = $DB->get_record('assign', [ 'id' => $submission->assignment ]);


        require_capability('assignsubmission/autograde:viewsubmission',
            context_course::instance($assignment->course), $USER->id, false);

        $result = new stdClass;
        $result->sid = $sid;
        $result->aid = $submission->assignment;
        $result->cid = $assignment->course;
        $result->attempt = $submission->attemptnumber;
        $result->submitters = assignsubmission_autograde_submitters($submission);

        return $result;
    }

    /**
     * ???
     * @return external_multiple_structure ???
     * @note Keep it in line with {@link Submission}
     */
    public static function info_submissions_returns(): external_single_structure {
        return new external_single_structure([
                'sid' => new external_value(PARAM_INT, 'The ID of the submission'),
                'aid' => new external_value(PARAM_INT, 'The ID of the assignment'),
                'cid' => new external_value(PARAM_INT, 'The ID of the course'),
                'submitters' => new external_multiple_structure(
                    new external_single_structure([
                        'id'=> new external_value(PARAM_INT, 'The ID of the user'),
                        'username' => new external_value(PARAM_TEXT, 'The username of the user'),
                        'firstname' => new external_value(PARAM_TEXT, 'The firstname of the user'),
                        'lastname' => new external_value(PARAM_TEXT, 'The lastname of the user'),
                        'idnumber' => new external_value(PARAM_TEXT, 'The lastname of the user'),
                        'email' => new external_value(PARAM_EMAIL,'The email of the user')

                ])),
                'attempt' => new external_value(PARAM_INT,'The attempt of the user'),
            ])
        ;
    }

}
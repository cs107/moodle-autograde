<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * This file defines a function to upload the feedback from the service
 *
 * @see https://moodledev.io/docs/apis/subsystems/external/functions
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright 2023 AUTOGRADE-EPFL <autograde-support@groupes.epfl.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace assignsubmission_autograde\external;

defined('MOODLE_INTERNAL') || die();

use context_course;
use context_user;
use dml_exception;
use external_api;
use external_function_parameters;
use external_value;
use moodle_exception;
use required_capability_exception;
use stdClass;
use function assignsubmission_autograde\utils\assignsubmission_autograde_get_assignment;

global $CFG;

require_once(__DIR__.'/../../lib.php');
require_once(__DIR__.'/../../utils.php');
require_once($CFG->libdir . '/externallib.php');
require_once($CFG->dirroot . '/mod/assign/locallib.php');


/**
 * ???
 *
 * @see https://moodledev.io/docs/apis/subsystems/external/functions
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright ???
 * @license ???
 */
final class autograde_upload_feedback extends external_api {

    /**
     * ???
     * @return external_function_parameters ???
     */
   public static function upload_feedback_parameters(): external_function_parameters
   {
       return new external_function_parameters([
           'submissionid' => new external_value(PARAM_INT, 'The ID of the submission'),
           'grade' => new external_value(PARAM_FLOAT, 'The grade to associate to the submission'),
           'files' => new external_value(PARAM_TEXT, "The feedback files to associate to the submission")
       ]);
   }

    /**
     * ???
     * @param $submissionid ???
     * @param $grade ???
     * @param $files
     * @return bool ???
     * @throws dml_exception ???
     * @throws moodle_exception
     * @throws required_capability_exception ???
     */
   public static function upload_feedback($submissionid, $grade, $files): bool{

       self::validate_parameters(self::upload_feedback_parameters(), [
           'submissionid' => $submissionid,
           'grade' => $grade,
           'files' => $files
       ]);

       global $DB, $USER;
       // HR : Fetch all the necessary ids to upload correctly the feedback
       $assignment_id = $DB->get_field('assign_submission', 'assignment', array('id' => $submissionid));
       $submission = $DB->get_record('assign_submission', array('id' => $submissionid));
       $course_id = $DB->get_field('assign', 'course', array('id' => $assignment_id));

       // HR : Check the necessary capabilities are present
       require_capability('assignsubmission/autograde:grade',
           context_course::instance($course_id), $USER->id, false);

       // HR : Fetch the assignment's object
       $assignment = assignsubmission_autograde_get_assignment($assignment_id);

       if($assignment->get_instance()->teamsubmission){
            $iiid = groups_get_members($submission->groupid);
            $user_id = reset($iiid)->id;
        } else {
            $user_id = $submission->userid;
        }

       // HR : Store the file feedback file
       $draftitemid = file_get_unused_draft_itemid();
       file_prepare_draft_area($draftitemid,
           context_user::instance($USER->id)->id,
           'assignfeedback_file',
           'feedback_files',
           1 // TODO HR : Can we just hardcode it here ?
       );
       $fs = get_file_storage();
       $files = json_decode($files);
       foreach ($files as $file){
           $draftinfo = array(
               'contextid' => context_user::instance($USER->id)->id,
               'component' => 'user',
               'filearea' => 'draft',
               'itemid' => $draftitemid,
               'filepath' => '/'.$files->path,
               'filename' => $file->filename
           );
           $fs->create_file_from_string($draftinfo, base64_decode($file->content));
       }

       // HR : We need to build the data
       $data = new stdClass();
       $data->addattempt = true;
       $data->attemptnumber = $submission->attemptnumber;
       #$data->workflowstate = $params['workflowstate'];
       $data->applytoall = true;
       $data->grade = $grade;
       $data->{'files_filemanager'} = $draftitemid;
       return $assignment->save_grade($user_id, $data);
   }

    /**
     * ???
     * @return external_value ???
     */
   public static function upload_feedback_returns(): external_value {
       return new external_value(PARAM_BOOL, 'Flag to verify if the assignment was graded');
   }
}

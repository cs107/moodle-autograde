<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * This file defines the function to download a submission
 *
 * @see https://moodledev.io/docs/apis/subsystems/external/functions
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright 2023 AUTOGRADE-EPFL <autograde-support@groupes.epfl.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace assignsubmission_autograde\external;

defined('MOODLE_INTERNAL') || die();

use assignsubmission_autograde\client\model\Submission;
use coding_exception;
use context_course;
use external_api;
use external_function_parameters;
use external_multiple_structure;
use external_single_structure;
use external_value;
use moodle_exception;
use stdClass;
use function assignsubmission_autograde\utils\assignsubmission_autograde_get_assignment;

global $CFG;

require_once(__DIR__.'/../../lib.php');
require_once(__DIR__.'/../../utils.php');
require_once($CFG->libdir . '/externallib.php');
require_once($CFG->dirroot . '/mod/assign/locallib.php');

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright ???
 * @license ???
 */
final class autograde_list_submissions extends external_api {

    /**
     * ???
     * @return external_function_parameters ???
     */
    public static function list_submissions_parameters(): external_function_parameters
    {
        return new external_function_parameters([
            'assignmentid' => new external_value(PARAM_INT, 'The ID of the assignment'),
        ]);
    }

    /**
     * ???
     * @param $assignmentid ???
     * @return stdClass[] ???
     * @throws coding_exception ???
     * @throws moodle_exception ???
     */
    public static function list_submissions($assignmentid): array {
        global $DB, $USER;
        self::validate_parameters(self::list_submissions_parameters(), ['assignmentid' => $assignmentid]);
        $course_id = $DB->get_field('assign', 'course', array('id' => $assignmentid));

        // HR : Check if the user has the necessary capabilities
        require_capability('assignsubmission/autograde:exportsubmissions',
            context_course::instance($course_id), $USER->id, false);

        // HR : Fetch the submissions from the DB
        $submissions = $DB->get_records('assign_submission', ['assignment' => $assignmentid]);
        $result = array();
        foreach ($submissions as $submission) {
            $entry = new stdClass();
            $entry->id      = $submission->id;
            $entry->attempt = $submission->attemptnumber;
            $result[] = $entry;
        }
        return $result;
    }

    /**
     * ???
     * @return external_multiple_structure ???
     * @note Keep it in line with {@link Submission}
     */
    public static function list_submissions_returns(): external_multiple_structure {
        return new external_multiple_structure(
            new external_single_structure([
                'id' => new external_value(PARAM_INT, 'The id of the submission'),
                'attempt' => new external_value(PARAM_INT, 'The attempt number for this submission'),
            ])
        );
    }

}
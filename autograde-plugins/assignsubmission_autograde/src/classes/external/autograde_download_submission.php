<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * This file defines the function to download a submission
 *
 * @see https://moodledev.io/docs/apis/subsystems/external/functions
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright 2023 AUTOGRADE-EPFL <autograde-support@groupes.epfl.ch>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace assignsubmission_autograde\external;

defined('MOODLE_INTERNAL') || die();

use coding_exception;
use context_course;
use external_api;
use external_function_parameters;
use external_multiple_structure;
use external_single_structure;
use external_value;
use moodle_exception;
use stdClass;
use function assignsubmission_autograde\utils\assignsubmission_autograde_get_assignment;

global $CFG;

require_once(__DIR__.'/../../lib.php');
require_once(__DIR__.'/../../utils.php');
require_once($CFG->libdir . '/externallib.php');
require_once($CFG->dirroot . '/mod/assign/locallib.php');

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @package assignsubmission_autograde
 * @copyright ???
 * @license ???
 */
final class autograde_download_submission extends external_api {

    /**
     * ???
     * @return external_function_parameters ???
     */
    public static function download_submission_parameters(): external_function_parameters
    {
        return new external_function_parameters([
            'submissionid' => new external_value(PARAM_INT, 'The ID of the submission'),
        ]);
    }

    /**
     * ???
     * @param $submissionid ???
     * @throws coding_exception ???
     * @throws moodle_exception ???
     */
    public static function download_submission($submissionid) {

        self::validate_parameters(self::download_submission_parameters(), ['submissionid' => $submissionid]);

        global $DB, $USER;
        // HR : Fetch the assignment's and course's ids linked to the assignment
        $assignment_id = $DB->get_field('assign_submission', 'assignment', array('id' => $submissionid));
        $course_id = $DB->get_field('assign', 'course', array('id' => $assignment_id));

        // HR : Check if the user has the necessary capabilities
        require_capability('assignsubmission/autograde:viewsubmissions',
            context_course::instance($course_id), $USER->id, false);

        // HR : Fetch the correct assignment instance
        $assignment = assignsubmission_autograde_get_assignment($assignment_id);
        $submission = $DB->get_record('assign_submission', ['id' => $submissionid]);

        // HR : Retrieve the files associated with the specified area and item
        $files = array();
        foreach ($assignment->get_submission_plugins() as $plugin){
            // TODO HR : Check if this condition is correct
            if($plugin->is_enabled() && $plugin->is_visible() && $plugin->allow_submissions())
                // TODO HR : The user here is not correctly built, stdClass...
                $files += $plugin->get_files($submission, new stdClass());
        }

        $files_to_zip = [];
        foreach ($files as $file) {
            $filepath = $file->get_filepath();
            $filename = $file->get_filename();
            $files_to_zip[$filepath . $filename] = $file;
        }

        //Prepare a temporary file for zipping
        $tempdir = make_temp_directory('submission_files') ;
        $zipfilename = "submission_{$submissionid}.zip" ;
        $tempzipfile = "$tempdir/$zipfilename" ;

        //Use Moodle's packer to create a zip file
        $packer = get_file_packer('application/zip') ;

        //Pack the files into the zip archive
        if (!$packer->archive_to_pathname($files_to_zip, $tempzipfile)) {
            throw new moodle_exception('couldnotcreatezip');
        }

        //Send the zip file as a response using Moodle's API
        send_temp_file($tempzipfile, $zipfilename);

    }

    /**
     * Do not remove this function since Moodle throws an error if it's missing
     * ???
     */
    public static function download_submission_returns(){
    }

}
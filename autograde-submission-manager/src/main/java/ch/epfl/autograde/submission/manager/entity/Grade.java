package ch.epfl.autograde.submission.manager.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public final class Grade {

    @JsonProperty
    private final float grade;

    @JsonCreator
    public Grade(@JsonProperty("grade") float grade){
        this.grade = grade;
    }

}

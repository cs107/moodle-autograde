package ch.epfl.autograde.submission.manager.visitor;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

import static java.nio.file.FileVisitResult.CONTINUE;
import static java.util.Objects.nonNull;

@Slf4j
public final class FeedbackVisitor implements FileVisitor<Path> {

    private final List<Path> collector;

    public FeedbackVisitor(List<Path> collector){
        this.collector = collector;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
        log.info("Collecting files in the '{}' directory", dir);
        return CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        log.info("Adding the following file as a feedback file : '{}'", file);
        collector.add(file);
        return CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        log.error("An error occurred when visiting the following file '{}'", file, exc);
        return CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
        if (nonNull(exc))
            log.error("An error occurred when discovering the '{}' directory", dir, exc);
        log.info("Finished discovering the files in the '{}' directory", dir);
        return CONTINUE;
    }
}

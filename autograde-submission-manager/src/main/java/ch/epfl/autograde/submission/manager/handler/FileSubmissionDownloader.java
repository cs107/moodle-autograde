package ch.epfl.autograde.submission.manager.handler;

import ch.epfl.autograde.client.v1.AutogradeClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import reactor.core.publisher.Mono;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;



/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
@Slf4j
public final class FileSubmissionDownloader {

    private static final String AUTOGRADE_SUBMISSION_PATH = "/data/submission";
    private static final String AUTOGRADE_FEEDBACK_PATH = "/data/feedback";

    private final AutogradeClient autograde;

    public FileSubmissionDownloader(AutogradeClient autograde) {
        this.autograde = autograde;
    }

    public void downloadSubmission(int id) throws IOException {
        //Ensure the submission and feedback directories exist
        try {
            Files.createDirectories(Path.of(AUTOGRADE_SUBMISSION_PATH));
            Files.createDirectories(Path.of(AUTOGRADE_FEEDBACK_PATH));
            log.info("Successfully ensured the /data/feedback and /data/submission folders exist");
        } catch (IOException e) {
            log.error("Creation of the submission and feedback folders failed", e);
            throw e;
        }

        // Fetch the ZIP file as a Mono<Resource>
        Resource resource = autograde.getMoodleSubmissionFiles(id);

        try (InputStream zipInputStream = resource.getInputStream()) {
            // Unzip the content
            unzip(zipInputStream, AUTOGRADE_SUBMISSION_PATH);
            log.info("Submission files for ID {} have been successfully downloaded and extracted.", id);
        } catch (UncheckedIOException e) {
            throw e.getCause();
        } catch (Exception e) {
            log.error("Failed to download and extract submission files for ID {}", id, e);
            throw new IOException(e);
        }
    }

    private void unzip(InputStream zipInputStream, String outputDir) throws IOException {
        File destDir = new File(outputDir);
        if (!destDir.exists()) {
            if (!destDir.mkdirs()) {
                throw new IOException("Failed to create output directory: " + outputDir);
            }
        }

        try (ZipInputStream zis = new ZipInputStream(zipInputStream)) {
            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null) {
                // Get the safe file path
                File newFile = newFile(destDir, entry.getName());

                if (entry.isDirectory()) {
                    // Create directories
                    if (!newFile.isDirectory() && !newFile.mkdirs()) {
                        throw new IOException("Failed to create directory: " + newFile);
                    }
                } else {
                    // Ensure parent directories exist
                    File parent = newFile.getParentFile();
                    if (!parent.isDirectory() && !parent.mkdirs()) {
                        throw new IOException("Failed to create directory: " + parent);
                    }

                    // Write file contents
                    try (FileOutputStream fos = new FileOutputStream(newFile)) {
                        byte[] buffer = new byte[4096];
                        int len;
                        while ((len = zis.read(buffer)) > 0) {
                            fos.write(buffer, 0, len);
                        }
                    } catch (IOException e) {
                        log.error("Failed to write file: {}", newFile, e);
                        throw e;
                    }
                }
                zis.closeEntry();
            }
        }
    }

    private static File newFile(File destinationDir, String entryName) throws IOException {
        File destFile = new File(destinationDir, entryName);

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        // Security check to prevent Zip Slip vulnerability
        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target directory: " + entryName);
        }

        return destFile;
    }

}

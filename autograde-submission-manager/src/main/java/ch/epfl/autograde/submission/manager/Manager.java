package ch.epfl.autograde.submission.manager;

import ch.epfl.autograde.client.v1.AutogradeClientFactory;
import ch.epfl.autograde.submission.manager.handler.FileFeedbackUploader;
import ch.epfl.autograde.submission.manager.handler.FileSubmissionDownloader;
import lombok.extern.slf4j.Slf4j;

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
@Slf4j
public final class Manager {

    public static void main(String[] args) {
        // HR : Fetch the parameters
        var url = args[0];
        var key = args[1];
        var mode = args[2];
        var id  = Integer.parseInt(args[3]);
        // HR : Build the client
        var client = AutogradeClientFactory.client(url, key);
        // HR : Process the request
        switch (mode){
            case "download" -> {
                log.info("Submission manager will download the files for the submission with id '{}' from '{}'", id, url);
                var downloader = new FileSubmissionDownloader(client);
                try { downloader.downloadSubmission(id); }
                catch (Exception e){
                    log.error("Submission manager failed to download the files for submission with id '{}'", id, e);
                }
            }
            case "upload" -> {
                var sig = args[4];
                log.info("Submission manager will upload the feedback for the submission with id '{}' to '{}'", id, url);
                var uploader = new FileFeedbackUploader(client);
                try { uploader.uploadFeedback(id, sig); }
                catch (Exception e){
                    log.error("Submission manager failed to upload the feedback for submission with id '{}'", id, e);
                }
            }
            default ->
                log.error("submission manager can only 'download' or 'upload' files for a submission");
        }
    }

}

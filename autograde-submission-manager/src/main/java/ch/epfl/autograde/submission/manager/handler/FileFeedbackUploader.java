package ch.epfl.autograde.submission.manager.handler;

import ch.epfl.autograde.client.v1.AutogradeClient;
import ch.epfl.autograde.client.v1.entity.MoodleFile;
import ch.epfl.autograde.client.v1.request.UploadFeedbackRequest;
import ch.epfl.autograde.submission.manager.entity.Grade;
import ch.epfl.autograde.submission.manager.visitor.FeedbackVisitor;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
@Slf4j
public final class FileFeedbackUploader {

    private static final Path AUTOGRADE_FEEDBACK_PATH = Paths.get("/data/feedback");

    private final AutogradeClient client;

    public FileFeedbackUploader(AutogradeClient client){
        this.client = client;
    }

    public void uploadFeedback(int id, String sig) {
        var grade = fetchGrade();
        var files = fetchFeedbackFiles();
        var request = UploadFeedbackRequest.builder()
                .grade(grade)
                .files(files)
                .build();
        client.uploadFeedbackFiles(id, sig, request);
    }

    private List<MoodleFile> fetchFeedbackFiles() {
        var files = fetchFiles();

        return files.stream()
                .filter(p -> !p.endsWith("grade.json"))
                .map(this::buildMoodleFile)
                .toList();
    }

    private float fetchGrade(){
        var path = AUTOGRADE_FEEDBACK_PATH.resolve("grade.json");
        try {
            return new ObjectMapper().readValue(path.toFile(), Grade.class).getGrade();
        } catch (Exception e) {
            log.error("Failed to read the '{}' file", path, e);
            return 0;
        }
    }

    private List<Path> fetchFiles() {
        var paths = new ArrayList<Path>();
        try {
            Files.walkFileTree(AUTOGRADE_FEEDBACK_PATH, new FeedbackVisitor(paths));
        } catch (Exception e) {
            log.error("An error occurred when fetching the feedback files", e);
        }
        return paths;
    }

    private MoodleFile buildMoodleFile(Path path){
        try {
            return new MoodleFile(
                    AUTOGRADE_FEEDBACK_PATH.relativize(path).toString(),
                    path.getFileName().toString(),
                    Files.probeContentType(path),
                    Base64.getEncoder().encodeToString(Files.readAllBytes(path))
            );
        } catch (IOException e) {
            log.error("An error occurred when trying to build the MoodleFile with path '{}'", path, e);
            throw new RuntimeException(e);
        }
    }

}

package ch.epfl.autograde.client.v1.response;

import ch.epfl.autograde.client.v1.entity.MoodleFile;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public final class DownloadSubmissionResponse {

    @JsonProperty
    private final int id;

    @JsonProperty
    private final List<MoodleFile> files;

    @JsonCreator
    public DownloadSubmissionResponse(
            @JsonProperty("id") int id,
            @JsonProperty("files") List<MoodleFile> files
    ){
        this.id = id;
        this.files = files;
    }

}

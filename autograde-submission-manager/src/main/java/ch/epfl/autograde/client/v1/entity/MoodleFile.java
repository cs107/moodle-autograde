package ch.epfl.autograde.client.v1.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
@Data
public final class MoodleFile {

    @JsonProperty
    private final String path;

    @JsonProperty
    private final String filename;

    @JsonProperty
    private final String mimetype;

    @JsonProperty
    private final String content;

    @JsonCreator
    public MoodleFile(
            @JsonProperty("path") String path,
            @JsonProperty("filename") String filename,
            @JsonProperty("mimetype") String mimetype,
            @JsonProperty("content") String content){
        this.path = path;
        this.filename = filename;
        this.mimetype = mimetype;
        this.content = content;
    }

}

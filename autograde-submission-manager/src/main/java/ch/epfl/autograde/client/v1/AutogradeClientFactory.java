package ch.epfl.autograde.client.v1;

import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

import java.time.Duration;

import static java.util.Objects.nonNull;
import static org.springframework.web.reactive.function.client.support.WebClientAdapter.forClient;

import reactor.netty.http.client.HttpClient;

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
public final class AutogradeClientFactory {
    private static final int MAX_IN_MEMORY_SIZE = 100 * 1024 * 1024; // 100 MiB

    /**
     * ???
     * @param url ???
     * @param token ???
     * @return ???
     */
    public static AutogradeClient client(String url, String token){

        var http = HttpClient.create()
                .httpResponseDecoder(spec ->
                        spec.maxHeaderSize(MAX_IN_MEMORY_SIZE)
                                .h2cMaxContentLength(MAX_IN_MEMORY_SIZE)
                );

        var builder = WebClient.builder()
                .baseUrl(url)
                .codecs(conf ->
                        conf.defaultCodecs().maxInMemorySize(MAX_IN_MEMORY_SIZE)
                )
                .clientConnector(new ReactorClientHttpConnector(http));

        // HR: Add the access token if provided
        if (nonNull(token))
            builder.defaultHeader("API-KEY", token);
        var factory = HttpServiceProxyFactory.builder(forClient(builder.build()))
                .blockTimeout(Duration.ofMinutes(1))
                .build();
        return factory.createClient(AutogradeClient.class);
    }

    /**
     * ???
     * @param url ???
     * @return ???
     */
    public static AutogradeClient client(String url){
        return client(url, null);
    }

}

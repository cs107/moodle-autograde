package ch.epfl.autograde.client.v1.request;

import ch.epfl.autograde.client.v1.entity.MoodleFile;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public final class UploadFeedbackRequest {

    @JsonProperty
    private final float grade;

    @JsonProperty
    private final List<MoodleFile> files;

}

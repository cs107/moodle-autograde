package ch.epfl.autograde.client.v1;

import ch.epfl.autograde.client.v1.request.UploadFeedbackRequest;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.GetExchange;
import org.springframework.web.service.annotation.PostExchange;
import reactor.core.publisher.Mono;

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
public interface AutogradeClient {

    // ============================================================================================
    // ========================================= PING =============================================
    // ============================================================================================

    /**
     * ???
     * @return ???
     */
    @GetExchange("/api/v1/ping/no-auth")
    String pingNoAuth();

    /**
     * ???
     * @return ???
     */
    @GetExchange("/api/v1/ping/auth")
    String pingWithAuth();

    // ============================================================================================
    // ======================================== SUBMISSION ========================================
    // ============================================================================================

    @GetExchange(
            url = "/api/v1/submission/{id}/files",
            accept = "application/zip")
    Resource getMoodleSubmissionFiles(@PathVariable("id") int id);

    @PostExchange("/api/v1/submission/{id}/feedback")
    void uploadFeedbackFiles(@PathVariable("id") int id, @RequestParam("signature") String signature, @RequestBody UploadFeedbackRequest body);

}

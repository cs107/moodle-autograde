SHELL := /bin/bash
.PHONY: add-host release-autograde release-moodle release-submission-manager

add-host:
	echo "127.0.0.1    moodle" | sudo tee -a /etc/hosts

rm-host:
ifeq ($(shell uname), Darwin)
	sudo sed -i '' '/moodle/d' /etc/hosts
else
	sudo sed -i '/moodle/d' /etc/hosts
endif

release-autograde:
	docker buildx build --platform linux/amd64,linux/arm64 --push -t autograde/autograde-service:1.2.2 autograde-service/

release-moodle:
	docker buildx build --platform linux/amd64,linux/arm64 --push -t autograde/moodle:1.2.2 autograde-plugins/assignsubmission_autograde/

release-submission-manager:
	docker buildx build --platform linux/amd64,linux/arm64 --push -t autograde/submission-manager:1.2.2 autograde-submission-manager/

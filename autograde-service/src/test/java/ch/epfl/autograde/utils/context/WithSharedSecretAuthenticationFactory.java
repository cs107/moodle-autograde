package ch.epfl.autograde.utils.context;

import ch.epfl.autograde.auth.token.SharedSecretAuthentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
public final class WithSharedSecretAuthenticationFactory implements WithSecurityContextFactory<WithSharedSecretAuthentication> {

    private final SecurityContextHolderStrategy securityContextHolderStrategy = SecurityContextHolder
        .getContextHolderStrategy();

    @Override
    public SecurityContext createSecurityContext(WithSharedSecretAuthentication annotation) {
        var authentication = new SharedSecretAuthentication(annotation.secret(), true);
        var context = this.securityContextHolderStrategy.createEmptyContext();
        context.setAuthentication(authentication);
        return context;
    }

}

package ch.epfl.autograde.utils.context;

import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = WithSharedSecretAuthenticationFactory.class)
public @interface WithSharedSecretAuthentication {

    String secret() default "";

}

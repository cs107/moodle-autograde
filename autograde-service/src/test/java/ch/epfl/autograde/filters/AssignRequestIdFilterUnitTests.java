package ch.epfl.autograde.filters;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.MDC;

import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.3.0
 * @see ch.epfl.autograde.filters.AssignRequestIdFilter
 */
@DisplayName("AssignRequestIdFilter Unit Tests")
@ExtendWith(MockitoExtension.class)
final class AssignRequestIdFilterUnitTests {

    private final AssignRequestIdFilter filter = new AssignRequestIdFilter();

    @Test @DisplayName("Filter adds the 'X-Request-Id' attribute to the HttpServletRequest")
    void filterSetsTheAttributeInTheRequest(
            @Mock HttpServletRequest request,
            @Mock HttpServletResponse response,
            @Mock FilterChain chain
    ) throws Exception {
        filter.doFilter(request, response, chain);
        verify(request).setAttribute(eq(AssignRequestIdFilter.REQUEST_ID_HEADER), notNull());
    }

    @Test @DisplayName("Filter adds the 'X-Request-Id' header to the HttpServletResponse")
    void filterSetsTheHeaderInTheResponse(
            @Mock HttpServletRequest request,
            @Mock HttpServletResponse response,
            @Mock FilterChain chain
    ) throws Exception {
        filter.doFilter(request, response, chain);
        verify(response).setHeader(eq(AssignRequestIdFilter.REQUEST_ID_HEADER), notNull());
    }

    @Test @DisplayName("Filter adds the 'X-Request-Id' to the MDC")
    void filterSetsMDCContext(
            @Mock HttpServletRequest request,
            @Mock HttpServletResponse response,
            @Mock FilterChain chain
    ) throws Exception {
        filter.doFilter(request, response, chain);
        assertNotNull(MDC.get(AssignRequestIdFilter.REQUEST_ID_HEADER));
    }

    @Test @DisplayName("Filter calls the rest of the FilterChain")
    void filterCallsTheRestOfTheChain(
            @Mock HttpServletRequest request,
            @Mock HttpServletResponse response,
            @Mock FilterChain chain
    ) throws Exception {
        filter.doFilter(request, response, chain);
        verify(chain).doFilter(request, response);
    }

    @Test
    @DisplayName("Filter generates a unique 'X-Request-Id'")
    void filterUsesSameRequestIdAcrossAll(
            @Mock HttpServletRequest request,
            @Mock HttpServletResponse response,
            @Mock FilterChain chain,
            @Captor ArgumentCaptor<String> requestCaptor,
            @Captor ArgumentCaptor<String> responseCaptor
    ) throws Exception {
        filter.doFilter(request, response, chain);

        verify(request).setAttribute(eq(AssignRequestIdFilter.REQUEST_ID_HEADER), requestCaptor.capture());
        verify(response).setHeader(eq(AssignRequestIdFilter.REQUEST_ID_HEADER), responseCaptor.capture());

        final var requestAttrId = requestCaptor.getValue();
        final var responseHeaderId = responseCaptor.getValue();
        final var mdcRequestId = MDC.get(AssignRequestIdFilter.REQUEST_ID_HEADER);

        assertAll(
                () -> assertNotNull(requestAttrId),
                () -> assertNotNull(responseHeaderId),
                () -> assertNotNull(mdcRequestId),
                () -> assertEquals(requestAttrId, responseHeaderId),
                () -> assertEquals(requestAttrId, mdcRequestId)
        );
    }

}

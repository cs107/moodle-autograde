package ch.epfl.autograde.filters;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.3.0
 * @see ch.epfl.autograde.filters.AssignRequestIdFilter
 */
@DisplayName("AssignRequestIdFilter Integration Tests")
@AutoConfigureMockMvc
@SpringBootTest
final class AssignRequestIdFilterIntegrationTests {

  @Autowired
  private MockMvc mockMvc;

  /**
   * @implNote We call a fake-endpoint to test:
   * <ul>
   *   <li>The filter is called when calling an API Endpoint</li>
   *   <li>The filter is executed before the authentication filters</li>
   *   <li>The filter is called before dispatching to the controller</li>
   * </ul>
   */
  @Test @DisplayName("Request to API endpoints should have a 'X-Request-Id' header")
  void apiRequestShouldHaveARequestID() throws Exception {
    mockMvc.perform(get("/api/v1/fake-endpoint"))
            .andExpect(header().exists(AssignRequestIdFilter.REQUEST_ID_HEADER));
  }

    /**
   * @implNote We call a fake-endpoint to test:
   * <ul>
   *   <li>The filter is not called when calling a non-API Endpoint</li>
   *   <li>The filter is executed before the authentication filters</li>
   *   <li>The filter is called before dispatching to the controller</li>
   * </ul>
   */
  @Test @DisplayName("Request to non-API endpoints should not have a 'X-Request-Id' header")
  void nonApiRequestShouldNotHaveARequestID() throws Exception {
    mockMvc.perform(get("/not-api/v1/fake-endpoint"))
            .andExpect(header().doesNotExist(AssignRequestIdFilter.REQUEST_ID_HEADER));
  }

}

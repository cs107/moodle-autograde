package ch.epfl.autograde.controller.api.v1;

import ch.epfl.autograde.auth.token.SharedSecretAuthentication;
import ch.epfl.autograde.filters.AssignRequestIdFilter;
import ch.epfl.autograde.utils.context.WithSharedSecretAuthentication;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the {@link PingController} API.
 * <p>
 * This class verifies the behaviour of the ping endpoint provided by the API:
 * <ul>
 *   <li>{@code /api/v1/ping} - An endpoint accessible without authentication.</li>
 * </ul>
 * It uses Spring Boot's {@link MockMvc} for testing HTTP requests and responses,
 * and the {@link WithSharedSecretAuthentication} annotation to simulate requests
 * with <i>shared secret</i> authentication.
 *
 * @see ch.epfl.autograde.controller.api.v1.PingController
 * @see SharedSecretAuthentication
 * @see ch.epfl.autograde.config.SecurityConfig
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
@SpringBootTest @AutoConfigureMockMvc
@DisplayName("[Integration Tests] '/api/v1/ping'")
public final class PingControllerIntegrationTests {

  /** The endpoint to perform a ping */
  private final static String PING_END_POINT = "/api/v1/ping";

  @Autowired
  private MockMvc mockMvc;

  /**
   * If <b>not</b> authenticated with the shared secret, the request to `GET:/api/v1/ping`
   * should be <b>successful</b>
   * <p>
   * The following test will verify that:
   * <ul>
   *     <li>The HTTP Status is {@link org.springframework.http.HttpStatus.OK}</li>
   *     <li>The {@link HttpHeaders.CONTENT_TYPE} is {@link MediaType.APPLICATION_JSON}</li>
   *     <li>The request will not generate cookies</li>
   *     <li>The {@code $.auth} field in the response's body is {@code false}</li>
   *     <li>A `X-Request-Id` header was set in the response</li>
   * </ul>
   */
  @Test @DisplayName("'GET:/api/v1/ping' without authentication works as expected")
  void successPingWithoutKey() throws Exception {
    mockMvc.perform(get(PING_END_POINT))
            .andExpectAll(
                    status().isOk(),
                    content().contentType(MediaType.APPLICATION_JSON),
                    header().doesNotExist(HttpHeaders.SET_COOKIE),
                    header().exists(AssignRequestIdFilter.REQUEST_ID_HEADER),
                    jsonPath("$.auth").value(false)
            );
  }

  /**
   * If authenticated with the shared secret, the request to `GET:/api/v1/ping`
   * should be <b>successful</b>
   * <p>
   * The following test will verify that:
   * <ul>
   *     <li>The HTTP Status is {@link org.springframework.http.HttpStatus.OK}</li>
   *     <li>The {@link HttpHeaders.CONTENT_TYPE} is {@link MediaType.APPLICATION_JSON}</li>
   *     <li>The request will not generate cookies</li>
   *     <li>The {@code $.auth} field in the response's body is {@code true}</li>
   *     <li>A `X-Request-Id` header was set in the response</li>
   * </ul>
   */
  @Test @DisplayName("'GET:/api/v1/ping' with authentication works as expected")
  @WithSharedSecretAuthentication
  void successPingWithKey() throws Exception {
    mockMvc.perform(get(PING_END_POINT))
            .andExpectAll(
                    status().isOk(),
                    content().contentType(MediaType.APPLICATION_JSON),
                    header().doesNotExist(HttpHeaders.SET_COOKIE),
                    header().exists(AssignRequestIdFilter.REQUEST_ID_HEADER),
                    jsonPath("$.auth").value(true)
            );
  }

}

package ch.epfl.autograde.controller.api.v1;

import ch.epfl.autograde.auth.token.SharedSecretAuthentication;
import ch.epfl.autograde.utils.context.WithSharedSecretAuthentication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test class for the {@link InfoController} API.
 * <p>
 * This class verifies the behaviour of the protected endpoint provided by the API:
 * <ul>
 *  <li>{@code /api/v1/info} - An endpoint that requires shared secret authentication.</li>
 * </ul>
 * It uses Spring Boot's {@link MockMvc} for testing HTTP requests and responses,
 * and the {@link WithSharedSecretAuthentication} annotation to simulate authenticated requests
 *
 * @see ch.epfl.autograde.controller.api.v1.InfoController
 * @see SharedSecretAuthentication
 * @see ch.epfl.autograde.config.SecurityConfig
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
@AutoConfigureMockMvc
@SpringBootTest
public final class InfoControllerTest {

    /** The endpoint to perform an authenticated ping */
    private final static String INFO_END_POINT = "/api/v1/info";

    @Autowired
    private MockMvc mockMvc;

    /**
     * If <b>not</b> authenticated with the shared secret, the request to `/api/v1/info`
     * should <b>fail</b>
     * <p>
     * The following test will verify that:
     * <ul>
     *     <li>The HTTP Status is {@link org.springframework.http.HttpStatus.UNAUTHORIZED}</li>
     * </ul>
     */
    @Test
    void failWithoutKey() throws Exception {
        mockMvc.perform(get(INFO_END_POINT))
                .andExpect(status().isUnauthorized())
        ;
    }

    /**
     * If authenticated with the shared secret, the request to `/api/v1/info`
     * should be <b>successful</b>
     * <p>
     * The following test will verify that:
     * <ul>
     *     <li>The HTTP Status is {@link org.springframework.http.HttpStatus.OK}</li>
     * </ul>
     */
    @Test
    @WithSharedSecretAuthentication
    void successWithKey() throws Exception {
        mockMvc.perform(get(INFO_END_POINT))
                .andExpect(status().isOk());
    }

    // TODO: We need to test authorization here when we implement it

}

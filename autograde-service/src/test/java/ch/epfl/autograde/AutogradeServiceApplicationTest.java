package ch.epfl.autograde;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AutogradeServiceApplicationTest {

	@Test
	@DisplayName("Can we correctly boot the application and load the context ?")
	void contextLoads() {}

}

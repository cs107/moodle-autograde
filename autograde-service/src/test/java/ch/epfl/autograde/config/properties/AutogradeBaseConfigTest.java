package ch.epfl.autograde.config.properties;

import ch.epfl.autograde.properties.AutogradeBaseConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@EnableConfigurationProperties(AutogradeBaseConfig.class)
@TestPropertySource("classpath:config/test-autograde-config.yaml")
final class AutogradeBaseConfigTest {

    @Autowired
    private AutogradeBaseConfig config;

    @Test
    void configShouldNotBeNull() {
        assertNotNull(config);
    }

    @Test
    void testBaseURL() {
        assertEquals("https://test.autograde.ch", config.baseUrl());
    }

    @Test
    void testVersion() {
        assertEquals("1.0.0", config.version());
    }

}

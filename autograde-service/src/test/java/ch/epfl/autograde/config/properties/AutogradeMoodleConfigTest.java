package ch.epfl.autograde.config.properties;

import ch.epfl.autograde.properties.AutogradeMoodleConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@EnableConfigurationProperties(AutogradeMoodleConfig.class)
@TestPropertySource("classpath:config/test-moodle-config.yaml")
final class AutogradeMoodleConfigTest {

    @Autowired
    private AutogradeMoodleConfig config;

    @Test
    void configShouldNotBeNull() {
        assertNotNull(config);
    }

    @Test
    void testBaseURL() {
        assertEquals("https://moodle.autograde.ch", config.baseUrl());
    }

    /*
    @Test
    void testAutogradeToken() {
        assertEquals("123456", config.autograde_token());
    }

    @Test
    void testGasparUsername() {
        assertEquals("username", config.gaspar_username());
    }

    @Test
    void testGasparPassword() {
        assertEquals("password", config.gaspar_password());
    }


     */
}

package ch.epfl.autograde.controller.api.v1;

import ch.epfl.autograde.model.response.PingResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

import static java.time.Instant.now;
import static java.util.Objects.nonNull;

/**
 * REST Controller to ping the service
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.0.0
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/ping")
@RequiredArgsConstructor
public final class PingController {

    /**
     * REST Endpoint to ping the service
     * @since 1.3.0
     * @return
     * <ul>
     *     <li>{@link HttpStatus.OK} if the request is successful</li>
     * </ul>
     */
    @GetMapping(consumes = MediaType.ALL_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PingResponse ping(Authentication authentication) {
        log.info("API Service received a ping");
        return new PingResponse(
                Date.from(now()),
                nonNull(authentication) && authentication.isAuthenticated()
        );
    }

}

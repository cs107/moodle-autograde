package ch.epfl.autograde.controller.api.v1;

import ch.epfl.autograde.model.response.InfoResponse;
import ch.epfl.autograde.service.AutogradeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST Controller to manage to report information on the service
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.0.0
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/info")
@RequiredArgsConstructor
public final class InfoController {

    /** Underlying autograde reflection service */
    private final AutogradeService autograde;

    /**
     * REST Endpoint to fetch global information on the service
     *
     * @since 1.0.0
     * @return
     * <ul>
     *     <li>{@link HttpStatus.OK} if the request is successful</li>
     * </ul>
     */
    @GetMapping
    public InfoResponse info(){
        log.info("Serving a request to get information about autograde");
        return new InfoResponse(
                autograde.getUrl(),
                autograde.getVersion()
        );
    }

}

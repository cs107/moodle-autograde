package ch.epfl.autograde.controller.api.v1;

import ch.epfl.autograde.model.request.CreateSubmissionRequest;
import ch.epfl.autograde.model.request.UploadFeedbackRequest;
import ch.epfl.autograde.model.response.SubmissionInfoResponse;
import ch.epfl.autograde.model.response.SubmissionStatusResponse;
import ch.epfl.autograde.model.response.CreateSubmissionResponse;
import ch.epfl.autograde.service.AutogradeService;
import ch.epfl.autograde.service.IntegrityService;
import ch.epfl.autograde.service.KubernetesJobService;
import ch.epfl.autograde.service.MoodleWebService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

/**
 * REST Controller for autograde submissions
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.0.0
 * @version 1.1.0
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/submission")
@RequiredArgsConstructor
public final class SubmissionController {

    /** Service to communicate with Moodle */
    private final MoodleWebService moodle;

    private final AutogradeService autograde;

    /** Service to generate and check the integrity of the requests */
    private final IntegrityService integrity;

    /** Service to communicate with the k8s cluster */
    private final KubernetesJobService k8s;

    /**
     * REST Endpoint to create a compatible submission
     *
     * @param request The payload of the request (see {@link CreateSubmissionRequest})
     * @since 1.1.0
     * @return
     * <ul>
     *     <li>{@link HttpStatus.CREATED} if the request is successful</li>
     *     <li>{@link HttpStatus.INTERNAL_SERVER_ERROR} in case of a server failure</li>
     * </ul>
     */
    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<CreateSubmissionResponse> create(final @RequestBody CreateSubmissionRequest request) {
        final var id = request.submission().id();
        log.info("Received request to create submission {}", id);
        log.debug("Processing the following request:\n{}", request);
        final var jobName = k8s.create_grading_submission_job(request);
        log.info("Successfully created submission {}", id);
        return ResponseEntity
                .created(URI.create(autograde.getUrl() + "/api/v1/submission/" + id))
                .body(new CreateSubmissionResponse(id, jobName));
    }

    /**
     * REST Endpoint to describe a submission
     *
     * @param id The unique id of the requested submission
     * @since 1.2.0
     * @return
     * <ul>
     *  <li>{@link HttpStatus.OK} if the request is successful</li>
     *  <li>{@link HttpStatus.NOT_FOUND} if the submission is missing</li>
     *  <li>{@link HttpStatus.INTERNAL_SERVER_ERROR} in case of a server failure</li>
     * </ul>
     */
    @GetMapping(value = "/{id}", produces = "application/json")
    public SubmissionInfoResponse get(final @PathVariable int id) {
        log.info("Received request to describe submission {}", id);

        return moodle.info_submission(id);
    }

    /**
     * REST Endpoint to check the status of a submission
     *
     * @param id The unique id of the requested submission
     * @since 1.1.0
     * @return
     * <ul>
     *     <li>{@link HttpStatus.OK} if the request is successful</li>
     *     <li>{@link HttpStatus.NOT_FOUND} if the submission is missing</li>
     *     <li>{@link HttpStatus.INTERNAL_SERVER_ERROR} in case of a server failure</li>
     * </ul>
     */
    @GetMapping(value = "/{id}/status", produces = "application/json")
    public SubmissionStatusResponse statusJson(final @PathVariable int id) {
        log.info("Received request to check the status of submission {}", id);
        var status = k8s.getSubmissionStatus(id);
        log.debug("Status of submission {} is '{}'", id, status.name());
        log.info("Successfully processed the status request for submission {}", id);
        return new SubmissionStatusResponse(id, status);
    }

    /**
     * REST Endpoint to fetch and download the files linked with the submission (student submission)
     *
     * @param id The unique id of the requested submission
     * @param signature An internally computed signature
     * @since 1.1.0
     * @apiNote This end point should only be called by the autograde-service itself
     * @return
     * <ul>
     *     <li>{@link HttpStatus.OK} if the request is successful and it has some content</li>
     *     <li>{@link HttpStatus.NO_CONTENT} if the request is successful and it has no content (no files to download) (TODO)</li>
     *     <li>{@link HttpStatus.NOT_FOUND} if the submission is missing (TODO)</li>
     *     <li>{@link HttpStatus.INTERNAL_SERVER_ERROR} in case of a server failure</li>
     * </ul>
     */
    @GetMapping(value = "/{id}/files", produces = "application/zip")
    public Resource download(final @PathVariable int id) {
        log.info("Processing request to download the files for submission {}", id);
        var files = moodle.download_submission(id);
        log.info("Successfully processed the request to download the files for submission {}", id);
        return new InputStreamResource(files);
    }

    /**
     * REST Endpoint to upload the feedback of a submission
     *
     * @param id The unique id of the requested submission
     * @param signature An internally computed signature
     * @param feedback The payload of the request (see {@link UploadFeedbackRequest})
     * @since 1.1.0
     * @apiNote This end point should only be called by the autograde-service itself
     * @return
     * <ul>
     *     <li>{@link HttpStatus.NO_CONTENT} if the request is successful</li>
     *     <li>{@link HttpStatus.BAD_REQUEST} if the signature doesn't correspond to the computed HMAC</li>
     *     <li>{@link HttpStatus.NOT_FOUND} if the submission is missing (TODO)</li>
     *     <li>{@link HttpStatus.INTERNAL_SERVER_ERROR} in case of a server failure</li>
     * </ul>
     */
    @PostMapping(value = "/{id}/feedback", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> feedback(final @PathVariable int id,
                                      final @RequestParam String signature,
                                      final @RequestBody UploadFeedbackRequest feedback)
    {
        log.info("Processing request to upload the feedback for submission {}", id);
        integrity.check(signature, id);
        var res = moodle.upload_feedback(id, feedback.grade(), feedback.files());
        if (res.statusCode() != HttpStatus.OK.value()) {
            // TODO: Clean the MoodleService API
            throw new RuntimeException("Moodle request returned with status: " + res.statusCode() + " " + res.body());
        } else {
            // HR : Request was successful
            log.info("Feedback was successfully stored on Moodle for submission with id {}", id);
            return ResponseEntity.noContent().build();
        }
    }

}

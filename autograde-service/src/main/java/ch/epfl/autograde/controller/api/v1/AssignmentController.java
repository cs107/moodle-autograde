package ch.epfl.autograde.controller.api.v1;

import ch.epfl.autograde.model.entity.Submission;
import ch.epfl.autograde.model.request.CreateAssignmentRequest;
import ch.epfl.autograde.service.KubernetesSecretService;
import ch.epfl.autograde.service.MoodleWebService;
import ch.epfl.autograde.service.AutogradeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

/**
 * REST Controller for autograde assignments
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.1.0
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/assignment")
@RequiredArgsConstructor
public final class AssignmentController {

    /** Service to communicate with the k8s cluster */
    private final KubernetesSecretService k8s;

    private final AutogradeService autograde;

    /** Service to communicate with Moodle */
    private final MoodleWebService moodle;

    /**
     * REST Endpoint to request the creation of an assignment
     *
     * @param request The payload of the request (see {@link CreateAssignmentRequest})
     * @since 1.1.0
     * @return
     * <ul>
     *     <li>{@link HttpStatus.CREATED} if the request is successful</li>
     *     <li>{@link HttpStatus.INTERNAL_SERVER_ERROR} in case of a server failure</li>
     * </ul>
     */
    @PostMapping(consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> create(final @RequestBody CreateAssignmentRequest request){
        final var id = request.assignment().id();
        log.info("Received request to create the assignment {}", id);
        log.debug("Processing the following request:\n{}", request);
        k8s.store_registry_credentials(id, request.registry());
        log.info("Successfully served request to create assignment {}", id);
        return ResponseEntity
                .created(URI.create(autograde.getUrl() + "/api/v1/assignment/" + id))
                .build();
    }

    /**
     * REST Endpoint to describe an assignment
     *
     * @param id The unique id of the requested assignment
     * @since 1.2.0
     * @return
     * <ul>
     *     <li>{@link HttpStatus.OK} if the request is successful</li>
     *     <li>{@link HttpStatus.NOT_FOUND} if the assignment is missing (TODO)</li>
     *     <li>{@link HttpStatus.INTERNAL_SERVER_ERROR} in case of a server failure</li>
     * </ul>
     */
    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<?> get(final @PathVariable int id){
        log.info("Received request to describe assignment {}", id);
        return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).build();
    }

    /**
     * REST Endpoint to request the deletion of an assignment
     *
     * @param id The unique id of the requested assignment
     * @since 1.1.0
     * @return
     * <ul>
     *     <li>{@link HttpStatus.NO_CONTENT} if the request is successful</li>
     *     <li>{@link HttpStatus.NOT_FOUND} if the assignment is missing (TODO)</li>
     *     <li>{@link HttpStatus.INTERNAL_SERVER_ERROR} in case of a server failure</li>
     * </ul>
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(final @PathVariable int id){
        log.info("Received request to delete the assignment {}", id);
        k8s.delete_registry_credentials(id);
        log.debug("Successfully deleted the assignment {}", id);
        return ResponseEntity.noContent().build();
    }

    /**
     * REST Endpoint to fetch the list of submissions linked to the assignment
     *
     * @param id The unique id of the requested assignment
     * @since 1.1.0
     * @return
     * <ul>
     *   <li>{@link HttpStatus.OK} if the request is successful</li>
     *   <li>{@link HttpStatus.NOT_FOUND} if the assignment is missing (TODO)</li>
     *   <li>{@link HttpStatus.INTERNAL_SERVER_ERROR} in case of a server failure</li>
     * </ul>
     */
    @GetMapping(value = "/{id}/submissions", produces = "application/json")
    public ResponseEntity<List<Submission>> submissions(final @PathVariable int id) {
        log.info("Received request to fetch the submissions for assignment {}", id);
        final var submissions = moodle.list_submissions(id);
        log.info("Successfully fetched the list of submissions for assignment {}", id);
        return ResponseEntity.status(HttpStatus.OK).body(submissions);
    }

    /**
     * REST Endpoint to download all submissions linked to the assignment in a zipped file
     *
     * @param id The unique id of the requested assignment
     * @since 1.1.0
     * @return
     * <ul>
     *   <li>{@link HttpStatus.OK} if the request is successful</li>
     *   <li>{@link HttpStatus.NOT_FOUND} if the assignment is missing (TODO)</li>
     *   <li>{@link HttpStatus.INTERNAL_SERVER_ERROR} in case of a server failure</li>
     * </ul>
     */
    @GetMapping(value = "/{id}/submissions", produces = "application/zip")
    public Resource download(final @PathVariable int id) {
        log.info("Received request to download the submissions for assignment {}", id);
        var files = moodle.download_all_submissions(id);
        log.info("Successfully download the submissions for assignment {}", id);
        return new InputStreamResource(files);
    }

}

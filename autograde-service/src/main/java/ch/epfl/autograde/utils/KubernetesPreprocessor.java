package ch.epfl.autograde.utils;

import lombok.extern.slf4j.Slf4j;

/**
 * A kubernetes preprocessor to enforce the kubernetes specification
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
@Slf4j
public final class KubernetesPreprocessor {

    /**
     * Preprocess label values to follow the kubernetes specification
     *
     * @param candidate A label value candidate to preprocess
     * @return A preprocessed value label
     */
    public static String preprocessLabel(String candidate) {
        log.debug("Preprocessing the '{}' label to match kubernetes' specification", candidate);
        candidate = candidate.replaceAll("[^A-Za-z0-9_.]+", "-"); // Remove illegal characters
        candidate = candidate
                .substring(0, Math.min(candidate.length(), 63))         // Reduce the length of the candidate
                .replaceAll("^[^a-zA-Z0-9]+", "")   // Strip illegal start characters
                .replaceAll("[^a-zA-Z0-9]+$", "");  // Strip illegal end characters

        if (!isValidLabelValue(candidate)) {
            log.error("KubernetesPreprocessor was not able to correctly preprocess the provided candidate");
            throw new IllegalStateException();
        }
        return candidate;
    }

    /**
     * Predicate to test if a candidate {@link String} value is a valid kubernetes label
     *
     * @author Hamza REMMAL (hamza.remmal@epfl.ch)
     * @see <a href="https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#syntax-and-character-set">Kubernetes Specification</a>
     */
    public static boolean isValidLabelValue(String candidate) {
        final var pattern ="(([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9])?";
        log.debug("Checking if '{}' is a valid kubernetes label", candidate);
        return candidate.length() < 64 && candidate.matches(pattern);
    }

}

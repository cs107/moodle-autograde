package ch.epfl.autograde.config;

import ch.epfl.autograde.properties.AutogradeKubernetesConfig;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.nio.file.Files;

/**
 * Configuration and setup of the underlying kubernetes cluster
 *
 * @author Dixit Sabharwal (dixit.sabharwal@epfl.ch)
 * @since 1.0
 */
@Slf4j
@Configuration
@RequiredArgsConstructor
public class KubernetesConfig {

    private final AutogradeKubernetesConfig config;

    /**
     * Provide a client to communicate to the underlying kubernetes cluster.
     * Uses the ServiceAccount specified in the pod spec to authenticate.
     */
    @Bean
    public KubernetesClient kubernetesClient(Config config) {
        return new KubernetesClientBuilder()
                .withConfig(config)
                .build();
    }

    @Bean
    protected Config config() throws IOException {
        if (config.config() != null) {
            log.info("Service will use a pre-configured kubeconfig to communicate with Kubernetes");
            final var cg = Config.fromKubeconfig(Files.readString(config.config()));
            cg.setMasterUrl("https://kubernetes:6443");
            return cg;
        } else {
            log.info("Service will auto-configure to communicate with Kubernetes");
            return Config.autoConfigure(null);
        }
    }

}

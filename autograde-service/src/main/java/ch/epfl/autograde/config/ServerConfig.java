package ch.epfl.autograde.config;

import ch.epfl.autograde.properties.AutogradeServerConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class ServerConfig implements WebServerFactoryCustomizer<TomcatServletWebServerFactory> {

    private final AutogradeServerConfig serverConfig;

    @Override
    public void customize(TomcatServletWebServerFactory factory) {
        factory.setPort(serverConfig.port());
    }

}

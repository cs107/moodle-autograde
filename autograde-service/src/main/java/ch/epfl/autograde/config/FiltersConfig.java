package ch.epfl.autograde.config;

import ch.epfl.autograde.filters.AssignRequestIdFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;

/**
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.3.0
 */
@Configuration
public class FiltersConfig {

  @Bean
  public FilterRegistrationBean<?> requestIdFilterRegistration(AssignRequestIdFilter filter) {
    final var registration = new FilterRegistrationBean<>();
    registration.setFilter(filter);
    // We should generate a request id only for api endpoints
    registration.addUrlPatterns("/api/v1/*");
    // Ensure that all the requests will have a request id by running the filter first
    registration.setOrder(Ordered.HIGHEST_PRECEDENCE);
    return registration;
  }

}

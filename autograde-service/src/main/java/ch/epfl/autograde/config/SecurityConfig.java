package ch.epfl.autograde.config;

import ch.epfl.autograde.auth.AutogradeAuthorities;
import ch.epfl.autograde.auth.ldap.EPFLAuthoritiesPopulator;
import ch.epfl.autograde.auth.token.SharedSecretAuthenticationProvider;
import ch.epfl.autograde.auth.token.SharedSecretConfigurer;
import ch.epfl.autograde.properties.AutogradeAuthConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.ldap.core.support.BaseLdapPathContextSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.RequestCacheConfigurer;
import org.springframework.security.config.ldap.LdapBindAuthenticationManagerFactory;
import org.springframework.security.ldap.DefaultSpringSecurityContextSource;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.servlet.util.matcher.MvcRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.web.servlet.handler.HandlerMappingIntrospector;

import static org.springframework.security.config.Customizer.withDefaults;

/**
 * Security configuration and setup of the autograde service.
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.0
 */
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {

    /**
     * ???
     * @param http ???
     * @return ???
     * @throws Exception ???
     */
    @Bean
    @Order(1)
    public SecurityFilterChain filterChain(HttpSecurity http,
                                           SharedSecretAuthenticationProvider provider,
                                           @Qualifier("ldapAuthenticationManager") AuthenticationManager manager
    ) throws Exception {
        return http
                .securityMatcher("/api/**")
                .with(new SharedSecretConfigurer<>(new ProviderManager(provider)), withDefaults())
                .httpBasic(withDefaults())
                .authenticationManager(manager)
                .csrf(AbstractHttpConfigurer::disable)
                .anonymous(AbstractHttpConfigurer::disable)
                .formLogin(AbstractHttpConfigurer::disable)
                .sessionManagement(AbstractHttpConfigurer::disable)
                .requestCache(RequestCacheConfigurer::disable)
                .rememberMe(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(auth -> {
                  // Do not enforce authentication for ping
                  auth.requestMatchers("/api/v1/ping").permitAll();
                  // By default, request authentication to access any end point
                  auth.anyRequest().authenticated();
                })
                .build();
    }

    @Bean
    @Order(2)
    public SecurityFilterChain actuatorSecurityFilterChain(HttpSecurity http) throws Exception {
        // TODO: Access should actually only be allowed to the cluster (and the admins ?)
        return http
                .securityMatcher("/actuator/**")
                .csrf(AbstractHttpConfigurer::disable)
                .requestCache(RequestCacheConfigurer::disable)
                .rememberMe(AbstractHttpConfigurer::disable)
                .anonymous(AbstractHttpConfigurer::disable)
                .httpBasic(AbstractHttpConfigurer::disable)
                .formLogin(AbstractHttpConfigurer::disable)
                .sessionManagement(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(auth -> {
                    auth.requestMatchers("/actuator/health").permitAll();
                    auth.anyRequest().authenticated();
                })
                .build();
    }

    @Bean
    @Order(Ordered.LOWEST_PRECEDENCE)
    public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http, @Qualifier("ldapAuthenticationManager") AuthenticationManager manager, HandlerMappingIntrospector introspector) throws Exception {
        return http
                // Match on anything but the api endpoints (these are authentication via the shared secret scheme only)
                .securityMatcher(new NegatedRequestMatcher(new MvcRequestMatcher(introspector, "/api/**")))
                .authenticationManager(manager)
                .formLogin(conf -> {
                  conf.loginPage("/login");
                  conf.usernameParameter("username");
                  conf.passwordParameter("password");
                  conf.permitAll();
                })
                .csrf(AbstractHttpConfigurer::disable)
                .anonymous(AbstractHttpConfigurer::disable)
                .requestCache(RequestCacheConfigurer::disable)
                .rememberMe(AbstractHttpConfigurer::disable)
                // Disable session management until we figure a way for replicated and distributed services
                .sessionManagement(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(auth -> {
                    auth.requestMatchers("/error", "/css/error-pages.css", "/images/favicons/**", "/images/logos/**", "/images/pictograms/**").permitAll();
                    auth.anyRequest().hasAuthority(AutogradeAuthorities.SYSTEM_ACCESS.getAuthority());
                })
                .logout(withDefaults())
                .build();
    }

    // ============================================================================================
    // ========================= EPFL LDAP AUTHENTICATION CONFIGURATION ===========================
    // ============================================================================================


    @Bean
    protected AuthenticationManager ldapAuthenticationManager(BaseLdapPathContextSource contextSource, AutogradeAuthConfig config, EPFLAuthoritiesPopulator populator) {
	    final var factory = new LdapBindAuthenticationManagerFactory(contextSource);
        factory.setUserSearchBase(config.ldap().userSearchBase());
	    factory.setUserSearchFilter(config.ldap().userSearchFilter());
        factory.setLdapAuthoritiesPopulator(populator);
	    return factory.createAuthenticationManager();
    }

    @Bean
    public BaseLdapPathContextSource contextSource(AutogradeAuthConfig config) {
	    return new DefaultSpringSecurityContextSource(config.ldap().source());
    }

}


package ch.epfl.autograde.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "autograde.server")
public record AutogradeServerConfig(
        int port
) {}

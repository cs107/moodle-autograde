package ch.epfl.autograde.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "autograde")
public record AutogradeBaseConfig(
        String baseUrl,
        String version
){}

package ch.epfl.autograde.properties;

import ch.epfl.autograde.model.ImagePullPolicy;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "autograde.manager")
public record AutogradeManagerConfig(
        String image,
        ImagePullPolicy pullPolicy
) {
}

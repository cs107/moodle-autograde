package ch.epfl.autograde.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "autograde.job")
public record AutogradeJobConfig (
        String namespace,
        String cpu,
        String memory,
        String volume,
        int ttl
){}


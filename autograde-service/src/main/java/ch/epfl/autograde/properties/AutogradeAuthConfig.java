package ch.epfl.autograde.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "autograde.auth")
public record AutogradeAuthConfig(
        AutogradeAPIConfig api,
        AutogradeLDAPConfig ldap
)
{
    public record AutogradeAPIConfig(String key,
                                     String integritySecret) {}

    public record AutogradeLDAPConfig(boolean enabled,
                                      String source,
                                      String userSearchBase,
                                      String userSearchFilter,
                                      String group) {}

}

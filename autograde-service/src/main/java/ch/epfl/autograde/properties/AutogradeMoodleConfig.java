package ch.epfl.autograde.properties;

import ch.epfl.autograde.model.BasicAuth;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.bind.ConstructorBinding;

import java.util.Optional;

@ConfigurationProperties(prefix = "autograde.moodle")
public record AutogradeMoodleConfig(
        String baseUrl,
        String token,
        Optional<BasicAuth> auth
) {

    @ConstructorBinding
    AutogradeMoodleConfig(String baseUrl, String token, BasicAuth auth) {
        this(baseUrl, token, Optional.ofNullable(auth));
    }

}

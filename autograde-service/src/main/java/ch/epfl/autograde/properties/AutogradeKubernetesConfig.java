package ch.epfl.autograde.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.nio.file.Path;

@ConfigurationProperties(prefix = "autograde.kubernetes")
public record AutogradeKubernetesConfig (
        Path config
) { }

package ch.epfl.autograde.properties;

import ch.epfl.autograde.model.ImagePullPolicy;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "autograde.job.image")
public record AutogradeJobImageConfig(
        ImagePullPolicy pullPolicy
){}

package ch.epfl.autograde.advice;


import ch.epfl.autograde.model.error.IncorrectProvidedSignature;
import ch.epfl.autograde.model.error.MissingRegistryCredentials;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice("ch.epfl.autograde.controller")
public final class DefaultControllerAdvice {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> exception(Exception exception) {
        log.info("Unexpected exception thrown", exception);
        return ResponseEntity
                .internalServerError()
                .build();
    }

    @ExceptionHandler(IncorrectProvidedSignature.class)
    public ResponseEntity<?> incorrectProvidedSignature(IncorrectProvidedSignature e) {
        log.error("HMAC verification failed for submission {}", e.id(), e);
        log.debug("HMAC verification for submission {} failed. '{}' was provided", e.id(), e.provided());
        return ResponseEntity
                .badRequest()
                .body("The provided signature is not valid");
    }

    @ExceptionHandler(MissingRegistryCredentials.class)
    public ResponseEntity<?> missingRegistryCredentials(MissingRegistryCredentials ex) {
        return ResponseEntity.internalServerError().build();
    }

}

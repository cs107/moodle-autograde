package ch.epfl.autograde.advice;


import ch.epfl.autograde.model.error.SubmissionNotFound;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice("ch.epfl.autograde")
public final class SubmissionControllerAdvice {

    @ExceptionHandler(SubmissionNotFound.class)
    public ResponseEntity<?> submissionNotFound(SubmissionNotFound exception) {
        log.error("Unable to find the submission with id {} for the following reason: {}",
                exception.id(),
                exception.reason());
        return ResponseEntity.notFound().build();
    }


}

package ch.epfl.autograde.filters;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.UUID;

/**
 * A filter that assigns a unique Request ID to every incoming HTTP request.
 *
 * <p>
 * This filter generates a unique {@code Request ID} for each HTTP request and ensures it is:
 * <ul>
 *     <li>Added to the request attributes, making it accessible to downstream components.</li>
 *     <li>Included as a header in the HTTP response ({@code X-Request-Id}).</li>
 *     <li>Logged in the Mapped Diagnostic Context (MDC) for consistent tracking in logs.</li>
 * </ul>
 * This filter is designed to run once per request and should be one of the earliest filters
 * in the filter chain.
 * </p>
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.3.0
 * @see ch.epfl.autograde.config.FiltersConfig#requestIdFilterRegistration(AssignRequestIdFilter)
 * @see org.springframework.web.filter.OncePerRequestFilter
 */
@Slf4j
@Component
@RequiredArgsConstructor
public final class AssignRequestIdFilter extends OncePerRequestFilter {

  public static final String REQUEST_ID_HEADER = "X-Request-Id";

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
    // Generate a unique Request ID
    // TODO: Extract the UUID to be a distributed service to have a real UUID and not an UUID per node
    final var requestId = UUID.randomUUID().toString();
    request.setAttribute(REQUEST_ID_HEADER, requestId);
    response.setHeader(REQUEST_ID_HEADER, requestId);
    MDC.put(REQUEST_ID_HEADER, requestId);
    log.trace("Generated Request-Id '{}' for request from '{}'", requestId, request.getRemoteAddr());
    chain.doFilter(request, response);
  }

}

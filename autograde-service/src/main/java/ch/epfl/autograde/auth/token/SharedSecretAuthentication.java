package ch.epfl.autograde.auth.token;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
public final class SharedSecretAuthentication implements Authentication {

    /** ??? */
    private boolean authenticated;

    /** ??? */
    private final String key;

    /**
     * ???
     * @param key ???
     * @param authenticated ???
     */
    public SharedSecretAuthentication(String key, boolean authenticated) {
        this.key           = key;
        this.authenticated = authenticated;
    }

    public String getKey(){
        return key;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return this;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        this.authenticated = isAuthenticated;
    }

    @Override
    public String getName() {
        return null;
    }
}

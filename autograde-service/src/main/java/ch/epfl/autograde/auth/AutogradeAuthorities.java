package ch.epfl.autograde.auth;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@RequiredArgsConstructor
public enum AutogradeAuthorities implements GrantedAuthority {
    SYSTEM_ACCESS("system:access");

    // ==============================================================

    private final String authority;

    @Override
    public java.lang.String getAuthority() {
        return authority;
    }
}

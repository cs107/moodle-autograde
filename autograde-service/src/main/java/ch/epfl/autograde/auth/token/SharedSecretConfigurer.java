package ch.epfl.autograde.auth.token;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.HttpSecurityBuilder;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.ExceptionHandlingConfigurer;
import org.springframework.security.config.annotation.web.configurers.HttpBasicConfigurer;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AnyRequestMatcher;

@RequiredArgsConstructor
public final class SharedSecretConfigurer<B extends HttpSecurityBuilder<B>> extends AbstractHttpConfigurer<HttpBasicConfigurer<B>, B> {

    private final AuthenticationManager manager;

    private final AuthenticationEntryPoint authenticationEntryPoint = new SharedSecretEntryPoint();

    @Override
    public void init(B http) throws Exception {
        super.init(http);
        final var exceptionHandling = http.getConfigurer(ExceptionHandlingConfigurer.class);
		if (exceptionHandling != null) {
			exceptionHandling.defaultAuthenticationEntryPointFor(postProcess(this.authenticationEntryPoint), AnyRequestMatcher.INSTANCE);
		}
    }

    @Override
    public void configure(B http) {
        final var filter  = new SharedSecretFilter(manager, this.authenticationEntryPoint);
        final var rememberMe = http.getSharedObject(RememberMeServices.class);
        if (rememberMe != null)
            filter.setRememberMeServices(rememberMe);
        filter.setSecurityContextHolderStrategy(getSecurityContextHolderStrategy());
        http.addFilterBefore(postProcess(filter), BasicAuthenticationFilter.class);
    }

}


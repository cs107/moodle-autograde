package ch.epfl.autograde.auth.ldap;

import ch.epfl.autograde.auth.AutogradeAuthorities;
import ch.epfl.autograde.properties.AutogradeAuthConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

import static java.util.Objects.nonNull;

@RequiredArgsConstructor
@Component
public final class EPFLAuthoritiesPopulator implements LdapAuthoritiesPopulator {

    private final AutogradeAuthConfig config;

    @Override
    public Collection<? extends GrantedAuthority> getGrantedAuthorities(DirContextOperations userData, String username) {
        final var groups = userData.getAttributes().get("memberof");
        if (nonNull(groups) && groups.contains(config.ldap().group()))
            return List.of(AutogradeAuthorities.SYSTEM_ACCESS);
        else
            return List.of();
    }

}

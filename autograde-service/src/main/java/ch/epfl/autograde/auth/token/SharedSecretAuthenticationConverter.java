package ch.epfl.autograde.auth.token;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.web.authentication.AuthenticationConverter;


public final class SharedSecretAuthenticationConverter implements AuthenticationConverter {

	@Override
	public SharedSecretAuthentication convert(HttpServletRequest request) {
		var header = request.getHeader("API-KEY");
		if (header == null) return null;
		header = header.trim();
		if (header.isEmpty()) {
			throw new BadCredentialsException("Empty authentication token");
		}
        return new SharedSecretAuthentication(header, false);
	}

}


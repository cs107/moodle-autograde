package ch.epfl.autograde.auth.token;

import ch.epfl.autograde.properties.AutogradeAuthConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import static java.util.Objects.isNull;

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
@Component
@RequiredArgsConstructor
public class SharedSecretAuthenticationProvider implements AuthenticationProvider {

    /** ??? */
    private final AutogradeAuthConfig config;

    /**
     * {@inheritDoc}
     * @param authentication the authentication request object.
     * @return ???
     * @throws AuthenticationException ???
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        var keyAuth = (SharedSecretAuthentication) authentication;
        if (isNull(keyAuth.getKey()))
            return authentication;
        if (keyAuth.getKey().equals(config.api().key()))
            return new SharedSecretAuthentication(keyAuth.getKey(), true);
        else
            throw new BadCredentialsException("API KEY IS NOT CORRECT");
    }

    /**
     * {@inheritDoc}
     * @param authenticationType ???
     * @return ???
     */
    @Override
    public boolean supports(Class<?> authenticationType) {
        return SharedSecretAuthentication.class.equals(authenticationType);
    }
}


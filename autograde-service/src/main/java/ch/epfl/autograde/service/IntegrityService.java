package ch.epfl.autograde.service;

import ch.epfl.autograde.model.error.IncorrectProvidedSignature;
import ch.epfl.autograde.properties.AutogradeAuthConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Value;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Service to compute and check the HMAC of a request
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.0
 */
@Slf4j
@Service
public final class IntegrityService {

    /** Algorithm to use when hashing */
    private static final String HMAC_ALGORITHM = "HmacSHA256";

    /** Randomly generated secret for hashing */
    private final SecretKey secret;

    public IntegrityService(AutogradeAuthConfig config){
        this.secret = new SecretKeySpec(config.api().integritySecret().getBytes(), HMAC_ALGORITHM);
    }

    /**
     * Generate the HMAC for a given submission ID and course ID
     * @param id ID to hash
     * @return the corresponding HMAC, null if the generation failed
     */
    public String generate(final int id) {
        log.info("Generating HMAC for submission {}", id);
        final var data = String.format("##%d##", id);
        try {
            final var mac = Mac.getInstance(HMAC_ALGORITHM);
            mac.init(secret);
            final var hmac = Base64.getEncoder()
                    .encodeToString(mac.doFinal(data.getBytes(UTF_8)));
            log.info("Successfully generated HMAC for submission {}", id);
            log.debug("Generated HMAC for submission {} is '{}'", id, hmac);
            return hmac;
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Cannot instantiate the HMAC generator, the configured algorithm is not available", e);
        } catch (InvalidKeyException e) {
            throw new IllegalStateException("Cannot initialise the HMAC generator, the configured key is not valid", e);
        }
    }

    /**
     * Check if the provided HMAC is valid
     *
     * @param hmac HMAC to verify
     * @param id id to verify
     */
    public void check(final String hmac, final int id) {
        log.info("Checking HMAC integrity for submission {}", id);
        log.debug("HMAC verification with HMAC='{}' and id={}", hmac, id);
        final var generated = generate(id);
        if (!generated.equals(hmac))
            throw new IncorrectProvidedSignature(id, hmac);
        log.info("HMAC validation was successful for submission {}", id);
    }

}

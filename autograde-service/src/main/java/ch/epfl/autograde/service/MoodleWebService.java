package ch.epfl.autograde.service;

import ch.epfl.autograde.model.entity.MoodleFile;
import ch.epfl.autograde.model.entity.Submission;
import ch.epfl.autograde.model.response.SubmissionInfoResponse;
import ch.epfl.autograde.properties.AutogradeMoodleConfig;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.net.URLEncoder.encode;
import static java.net.http.HttpResponse.BodyHandlers.ofInputStream;
import static java.net.http.HttpResponse.BodyHandlers.ofString;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Service to communicate with Moodle.
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.0.0
 */
@Slf4j
@Service
@RequiredArgsConstructor
public final class MoodleWebService {

    /** Moodle's end-point to receive REST requests */
    private final static String MOODLE_WEB_SERVICE_API = "/webservice/rest/server.php";

    /** Config properties for Moodle */
    private final AutogradeMoodleConfig properties;

    private final ObjectMapper mapper;

    // ============================================================================================
    // ===================================== GENERIC FUNCTION =====================================
    // ============================================================================================

    /**
     * ???
     * @param function ???
     * @param params ???
     * @return ???
     * @throws URISyntaxException ???
     * @throws IOException ???
     * @throws InterruptedException ???
     */
    private <T> HttpResponse<T> call(String function, Map<String, ?> params, HttpResponse.BodyHandler<T> handler)
            throws URISyntaxException, IOException, InterruptedException {

        var client = HttpClient.newHttpClient();

        // HR : Build the URL
        var sb = new StringBuilder(properties.baseUrl());
        sb.append(MOODLE_WEB_SERVICE_API); // HR : End point of moodle
        sb.append('?');
        sb.append("wstoken=").append(properties.token());
        sb.append("&wsfunction=").append(function);
        sb.append("&moodlewsrestformat=json");

        var body = params.entrySet()
                .stream()
                .map(entry -> String.format("%s=%s", encode(entry.getKey(), UTF_8), encode(entry.getValue().toString(), UTF_8)))
                .collect(Collectors.joining("&"));

        var request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .header("Content-Type", "application/x-www-form-urlencoded")
                .uri(new URI(sb.toString()));

        properties.auth().ifPresent(auth -> {
            log.info("Adding the basic-auth header with the gaspar account information");
            request.header("Authorization", auth.toHeaderToken());
        });

        // HR : Send the request to moodle and wait for the response
        return client.send(request.build(), handler);
    }

    // ============================================================================================
    // ================================== AUTOGRADE FUNCTIONS =====================================
    // ============================================================================================

    /**
     * Upload the feedback for an autograde compatible submission.
     * @param submissionid ???
     * @param grade ???
     * @param feedback ???
     * @return ???
     * @throws URISyntaxException ???
     * @throws IOException ???
     * @throws InterruptedException ???
     */
    public HttpResponse<?> upload_feedback(int submissionid, float grade, List<MoodleFile> feedback) {
        log.info("Uploading feedback for submission with id {}", submissionid);

        final var FUNCTION_NAME = "mod_assignsubmission_autograde_upload_feedback";

        try {

            final var params = Map.of(
                    "submissionid", submissionid,
                    "grade", grade,
                    "files", mapper.writeValueAsString(feedback)
            );
            // HR : Call the moodle web service
            var response = call(FUNCTION_NAME, params, ofString());
            //log.info("call to moodle_upload_feedback returned {}", response.body());
            return response;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Download an autograde compatible submission from Moodle
     * @param submissionid ???
     * @return ???
     * @throws URISyntaxException ???
     * @throws IOException ???
     * @throws InterruptedException ???
     */
    public InputStream download_submission(int submissionid) {
        log.info("Downloading submission with id {} from Moodle ({})", submissionid, properties.baseUrl());

        final var FUNCTION_NAME = "mod_assignsubmission_autograde_download_submission";

        final var params = Map.of(
                "submissionid", submissionid
        );

        try {
            var response = call(FUNCTION_NAME, params, ofInputStream());

            // HR : Check the http status
            if(response.statusCode() != HttpStatus.OK.value()){
                log.error("call to the moodle-autograde api failed - download_submission - id {}", submissionid);
                throw new IllegalStateException();
            }
            // HR : Deserialize the request
            return response.body();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public InputStream download_all_submissions(int assignmentid) {
        log.info("Downloading all submissions of assignment with id {} from Moodle ({})", assignmentid, properties.baseUrl());

        final var FUNCTION_NAME = "mod_assignsubmission_autograde_download_all_submissions";

        final var params = Map.of(
                "assignmentid", assignmentid
        );

        try {
            var response = call(FUNCTION_NAME, params, ofInputStream());

            // HR : Check the http status
            if(response.statusCode() != HttpStatus.OK.value()){
                log.error("call to the moodle-autograde api failed - download_all_submissions - id {}", assignmentid);
                throw new IllegalStateException();
            }
            // HR : Deserialize the request
            return response.body();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    public List<Submission> list_submissions(int id) {
        log.info("Listing the submissions for assignment with id '{}' from Moodle", id);

        final var FUNCTION_NAME = "mod_assignsubmission_autograde_list_submissions";

        final var params = Map.of("assignmentid", id);

        try {
            var response = call(FUNCTION_NAME, params, ofString(UTF_8));
            return mapper.readValue(response.body(), new TypeReference<>() {});
        } catch (URISyntaxException | IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public SubmissionInfoResponse info_submission(int submissionid) {
        log.info("Downloading submission with id {} from Moodle ({})", submissionid, properties.baseUrl());

        final var FUNCTION_NAME = "mod_assignsubmission_autograde_submission_info";

        final var params = Map.of(
                "sid", submissionid
        );

        try {
            var response = call(FUNCTION_NAME, params, ofString(UTF_8));

            // HR : Check the http status
            if(response.statusCode() != HttpStatus.OK.value()){
                log.error("call to the moodle-autograde api failed - download_submission - id {}", submissionid);
                throw new IllegalStateException();
            }
            // HR : Deserialize the request
            return mapper.readValue(response.body(), new TypeReference<>() {});
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}

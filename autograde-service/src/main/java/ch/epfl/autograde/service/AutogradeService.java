package ch.epfl.autograde.service;

import ch.epfl.autograde.properties.AutogradeAuthConfig;
import ch.epfl.autograde.properties.AutogradeBaseConfig;
import ch.epfl.autograde.properties.AutogradeJobConfig;
import ch.epfl.autograde.properties.AutogradeManagerConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Service to provide information about the running instance of autograde
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 * @since 1.0.0
 */
@Slf4j
@Service
@RequiredArgsConstructor
public final class AutogradeService {

    /** Config properties loaded from the system environment */
    private final AutogradeBaseConfig base;
    private final AutogradeAuthConfig authConfig;
    private final AutogradeManagerConfig manager;
    private final AutogradeJobConfig jobConfig;

    /**
     * Provide the base url to access the running autograde instance
     * @return autograde's base url
     */
    public String getUrl(){
        return base.baseUrl();
    }

    /**
     * Provide the version of the running autograde instance
     * @return autograde's version
     */
    public String getVersion(){
        return base.version();
    }

    /**
     * Provide the submission manager image used by the running autograde instance
     * @return autograde' submission manager image
     */
    public String getSubmissionManager(){
        return manager.image();
    }

    public String api_key(){
        return authConfig.api().key();
    }

    // ============================================================================================
    // ================================== JOB CONFIGURATION =======================================
    // ============================================================================================

    public String getJobVolumeSize() {
        return jobConfig.volume();
    }

    public String getJobMemorySize() {
        return jobConfig.memory();
    }

    public String getJobCPUSize() {
        return jobConfig.cpu();
    }

}

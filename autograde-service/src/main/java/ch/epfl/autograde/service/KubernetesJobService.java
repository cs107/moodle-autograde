package ch.epfl.autograde.service;

import ch.epfl.autograde.model.entity.Environment;
import ch.epfl.autograde.model.SubmissionStatus;
import ch.epfl.autograde.model.entity.User;
import ch.epfl.autograde.model.error.MissingRegistryCredentials;
import ch.epfl.autograde.model.error.SubmissionNotFound;
import ch.epfl.autograde.model.request.CreateSubmissionRequest;
import ch.epfl.autograde.properties.AutogradeJobConfig;
import ch.epfl.autograde.properties.AutogradeJobImageConfig;
import ch.epfl.autograde.properties.AutogradeManagerConfig;
import ch.epfl.autograde.utils.KubernetesPreprocessor;
import io.fabric8.kubernetes.api.model.*;
import io.fabric8.kubernetes.api.model.batch.v1.Job;
import io.fabric8.kubernetes.api.model.batch.v1.JobBuilder;
import io.fabric8.kubernetes.api.model.batch.v1.JobSpecBuilder;
import io.fabric8.kubernetes.client.KubernetesClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.String.valueOf;

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
@Slf4j
@Service
@RequiredArgsConstructor
public final class KubernetesJobService {

    /** Client to communicate with the underlying k8s cluster */
    private final KubernetesClient k8s;

    private final KubernetesSecretService secrets;

    /** Service to generate and check the integrity of the requests */
    private final IntegrityService integrity;

    private final AutogradeService autograde;

    /** Namespace to run the jobs in - where the current instance of autograde lives in */
    private final AutogradeJobConfig jobConfig;

    private final AutogradeJobImageConfig jobImageConfig;

    private final AutogradeManagerConfig managerConfig;

    public String create_grading_submission_job(final CreateSubmissionRequest request) {
        final var sid = request.submission().id();
        final var aid = request.assignment().id();
        log.info("Creating a kubernetes job for submission {}", sid);
        final var jobName     = computeJobNameForSubmission(sid);
        final var credentials = secrets.credentials_secret_name(aid);

        if (!secrets.isSecretPresent(credentials)) throw new MissingRegistryCredentials(aid);

        final var volume = generateVolumeJobDefinition(jobName);
        final var initContainer = prepare_init_container(volume, sid);
        final var gradingContainer = prepare_grading_container(volume, request.environment(),request);
        final var reportingContainer = prepare_feedback_container(volume, sid);
        final var job = create_job_object(
                jobName, volume, credentials,
                initContainer, gradingContainer, reportingContainer,
                request);
        try {
            log.info("Submitting Job definition for submission {} to the cluster", sid);
            log.debug("Kubernetes Job definition for submission {}:\n{}", sid, job);
            k8s.batch()
                    .v1()
                    .jobs()
                    .inNamespace(jobConfig.namespace())
                    .resource(job)
                    .create();
        } catch (Exception e) {
            log.error("Failed to create job '{}' for submission {}", jobName, sid, e);
            throw new RuntimeException("Failed to create job");
        }

        return jobName;
    }

    // ============================================================================================
    // =========================================== UTILS ==========================================
    // ============================================================================================

    /**
     * Compute the name of the Job for a given submission
     *
     * @param id the unique id of the submission
     * @return the generated name for the job
     */
    private String computeJobNameForSubmission(final int id) {
        log.debug("Generating the name of the job for submission {}", id);
        final var name = String.format("grader-%d-%s", id,
                LocalDateTime.now().toString()
                        .replace(':', '-')
                        .replace('.', '-')
                        .replace('T', '-')
        );
        log.debug("Generated job name '{}' for submission {}", name, id);
        return name;
    }

    // ============================================================================================
    // ======================================== QUERIES ===========================================
    // ============================================================================================

    public SubmissionStatus getSubmissionStatus(int id) {
        final var pod = getJobsPodFromSubmissionId(id);
        return transformK8sStatusToAutogradeStatus(pod.getStatus());
    }

    private Pod getJobsPodFromSubmissionId(int id) throws SubmissionNotFound {
        final var submissionPods = k8s.pods()
                .inNamespace(jobConfig.namespace())
                .withLabel("submission.id", valueOf(id))
                .list()
                .getItems();

        if (submissionPods.size() == 1) {
            return submissionPods.get(0);
        } else if (submissionPods.isEmpty()) {
            throw new SubmissionNotFound(id, "No pods were found for the queried submission");
        } else {
            throw new IllegalStateException("Too many matched pods for submission with id: " + id);
        }
    }

    // ============================================================================================
    // ======================================= TRANSFORMERS =======================================
    // ============================================================================================

    private SubmissionStatus transformK8sStatusToAutogradeStatus(PodStatus k8s) {
        final var initContainers = k8s.getInitContainerStatuses();
        final var containers = k8s.getContainerStatuses();

        if (initContainers.size() != 2 || containers.size() != 1)
            throw new IllegalStateException("Underlying kubernetes job is not correctly configured");

        final var initState    = initContainers.get(0).getState();
        final var gradingState = initContainers.get(1).getState();
        final var cleanupState = containers.get(0).getState();

        log.debug("initStatus: {} - {} - {}", initState.getWaiting(), initState.getRunning(), initState.getTerminated());
        log.debug("gradingState: {} - {} - {}", gradingState.getWaiting(), gradingState.getRunning(), gradingState.getTerminated());
        log.debug("cleanupState: {} - {} - {}", cleanupState.getWaiting(), cleanupState.getRunning(), cleanupState.getTerminated());

        if (initState.getWaiting() != null) {
            // We are still waiting for the init container to start, the submission is still queued
            return SubmissionStatus.WAITING;
        } else if (initState.getRunning() != null) {
            // The init container is running, this is great !!
            return SubmissionStatus.PULLING_SUBMISSION;
        } else if (initState.getTerminated() != null && !initState.getTerminated().getReason().equals("Completed")) {
            // The init container failed to pull the submissions...
            return SubmissionStatus.FAILED;
        } else if (gradingState.getWaiting() != null) {
            // The grader container waits to be scheduled...
            return SubmissionStatus.WAITING;
        } else if (gradingState.getRunning() != null) {
            // The grader is running...
            return SubmissionStatus.GRADING;
        } else if (gradingState.getTerminated() != null && !gradingState.getTerminated().getReason().equals("Completed")) {
            // The grader container failed to grade the submission...
            return SubmissionStatus.FAILED;
        } else if (cleanupState.getWaiting() != null) {
            return SubmissionStatus.WAITING;
        } else if (cleanupState.getRunning() != null) {
            return SubmissionStatus.UPLOADING_FEEDBACK;
        } else if (cleanupState.getTerminated() != null && !cleanupState.getTerminated().getReason().equals("Completed")) {
            return SubmissionStatus.FAILED;
        } else
            return SubmissionStatus.COMPLETED;
    }

    /**
     * Generating an ephemeral volume definition
     *
     * @param jobName the prefix of the volume's name
     * @return An ephemeral volume definition
     */
    private Volume generateVolumeJobDefinition(final String jobName) {
        log.debug("Generating ephemeral Volume definition for job '{}'", jobName);
        final var volume = new VolumeBuilder()
                .withName(jobName + "-volume")
                .withEmptyDir(new EmptyDirVolumeSourceBuilder()
                        .withNewSizeLimit(autograde.getJobVolumeSize())
                        .build())
                .build();
        log.debug("Generated ephemeral Volume definition for job '{}':\n{}", jobName, volume);
        return volume;
    }

    /**
     * ???
     *
     * @param volume ???
     * @return ???
     */
    private Container prepare_init_container(Volume volume, int id) {
        return new ContainerBuilder()
                .withName("init")
                .withImage(autograde.getSubmissionManager())
                .withImagePullPolicy(managerConfig.pullPolicy().toString())
                .withCommand("java", "-jar", "autograde-submission-manager.jar",
                        autograde.getUrl(),
                        autograde.api_key(),
                        "download",
                        valueOf(id)
                )
                .withVolumeMounts(new VolumeMountBuilder()
                        .withName(volume.getName())
                        .withMountPath("/data")
                        .build())
                .withResources(new ResourceRequirementsBuilder()
                        .withRequests(Map.of(
                                "cpu", new Quantity(autograde.getJobCPUSize()),
                                "memory", new Quantity(autograde.getJobMemorySize())))
                        .withLimits(Map.of(
                                "cpu", new Quantity(autograde.getJobCPUSize()),
                                "memory", new Quantity(autograde.getJobMemorySize())))
                        .build())
                .build();
    }

    /**
     * Generates the container definition for the grading container.
     *
     * @param volume volume with the `submission` and `grade` directories
     * @param env Configuration of the grading environment
     * @return container definition
     */
    private Container prepare_grading_container(Volume volume, Environment env, CreateSubmissionRequest request) {
        return new ContainerBuilder()
                .withName("grader")
                .withImage(env.image().trim())
                .withImagePullPolicy(jobImageConfig.pullPolicy().toString())
                .withVolumeMounts(new VolumeMountBuilder()
                        .withName(volume.getName())
                        .withMountPath("/data")
                        .build())
                .withResources(new ResourceRequirementsBuilder()
                        .withRequests(Map.of(
                                "cpu", new Quantity(autograde.getJobCPUSize()),
                                "memory", new Quantity(autograde.getJobMemorySize())))
                        .withLimits(Map.of(
                                "cpu", new Quantity(autograde.getJobCPUSize()),
                                "memory", new Quantity(autograde.getJobMemorySize())))
                        .build())
                .withEnv(buildJobEnvironmentVariables(request))
                .withSecurityContext(new SecurityContextBuilder()
                        .withCapabilities(new CapabilitiesBuilder()
                                .addToAdd("NET_ADMIN")
                                .build())
                        .build())
                .build();
    }

    /**
     * ???
     *
     * @param volume ???
     * @return ???
     */
    private Container prepare_feedback_container(Volume volume, int id) {
        var sig = integrity.generate(id);
        return new ContainerBuilder()
                .withName("cleanup")
                .withImage(autograde.getSubmissionManager())
                .withImagePullPolicy(managerConfig.pullPolicy().toString())
                .withCommand("java", "-jar", "autograde-submission-manager.jar",
                        autograde.getUrl(),
                        autograde.api_key(),
                        "upload",
                        valueOf(id),
                        sig
                )
                .withVolumeMounts(new VolumeMountBuilder()
                        .withName(volume.getName())
                        .withMountPath("/data")
                        .build())
                .withResources(new ResourceRequirementsBuilder()
                        .withRequests(Map.of(
                                "cpu", new Quantity(autograde.getJobCPUSize()),
                                "memory", new Quantity(autograde.getJobMemorySize())))
                        .withLimits(Map.of(
                                "cpu", new Quantity(autograde.getJobCPUSize()),
                                "memory", new Quantity(autograde.getJobMemorySize())))
                        .build())
                .build();
    }

    /**
     * ???
     *
     * @param job_name           ???
     * @param volume             ???
     * @param imagePullSecretRef ???
     * @param initContainer      ???
     * @param gradingContainer   ???
     * @param reportingContainer ???
     * @param request            ???
     * @return ???
     */
    private Job create_job_object(
            String job_name, Volume volume, String imagePullSecretRef,
            Container initContainer, Container gradingContainer, Container reportingContainer,
            CreateSubmissionRequest request
    ) {
        final var labels = buildKubernetesJobLabels(request) ;

        return new JobBuilder()
                .withApiVersion("batch/v1")
                .withKind("Job")
                .withMetadata(new ObjectMetaBuilder()
                        .withName(job_name)
                        .withLabels(labels)
                        .build())
                .withSpec(new JobSpecBuilder()
                        .withTemplate(new PodTemplateSpecBuilder()
                                .withMetadata(new ObjectMetaBuilder()
                                        .withLabels(labels)
                                        .build())
                                .withSpec(new PodSpecBuilder()
                                        .withRestartPolicy("OnFailure")
                                        .addNewImagePullSecret(imagePullSecretRef)
                                        .addNewImagePullSecret("regcred-deployments")
                                        .withInitContainers(initContainer, gradingContainer)
                                        .withContainers(reportingContainer)
                                        .withVolumes(volume)
                                        .build())
                                .build())
                        .withBackoffLimit(2) // failed jobs can be retried twice
                        .withTtlSecondsAfterFinished(jobConfig.ttl())
                        .build())
                .build();
    }

    /**
     * Build the list of labels to add to the Kubernetes Job
     * @param request The grading request as received by the service
     * @return The list of labels to add to the Kubernetes Job
     */
    private Map<String,String> buildKubernetesJobLabels(final CreateSubmissionRequest request) {
        final var labels = new HashMap<String, String>();
        labels.put("autograde.version", autograde.getVersion());
        labels.put("course.short-name", KubernetesPreprocessor.preprocessLabel(request.course().shortName()));
        labels.put("user.identifier", KubernetesPreprocessor.preprocessLabel(joinUsersIdNumber(request.users())));
        labels.put("assignment.id", KubernetesPreprocessor.preprocessLabel(valueOf(request.assignment().id())));
        labels.put("submission.id", KubernetesPreprocessor.preprocessLabel(valueOf(request.submission().id())));
        labels.put("submission.attempt", KubernetesPreprocessor.preprocessLabel(valueOf(request.submission().attempt())));
        return Map.copyOf(labels);
    }

    /**
     * Build the list of environment variables to pass to the <b>grading container</b>
     * @param request The grading request as received by the service
     * @return The list of environment variables to use by the <b>grading container</b>
     */
    private List<EnvVar> buildJobEnvironmentVariables(final CreateSubmissionRequest request) {
        final var variables = new HashMap<String, String>();
        // Add all the autograde variables
        variables.put("AUTOGRADE_VERSION", autograde.getVersion());
        variables.put("AUTOGRADE_COURSE_SHORTNAME", request.course().shortName());
        variables.put("AUTOGRADE_USER_IDENTIFIER", joinUsersIdNumber(request.users()));
        variables.put("AUTOGRADE_ASSIGNMENT_ID", valueOf(request.assignment().id()));
        variables.put("AUTOGRADE_SUBMISSION_ID", valueOf(request.submission().id()));
        variables.put("AUTOGRADE_SUBMISSION_ATTEMPTNUMBER", valueOf(request.submission().attempt()));
        // Add all the environment variables from the user
        variables.putAll(request.environment().variables());
        // Build K8S representation of the variables
        return variables.entrySet().stream()
                .map(val -> new EnvVarBuilder()
                        .withName(val.getKey())
                        .withValue(val.getValue())
                        .build())
                .collect(Collectors.toList());
    }

    // ============================================================================================
    // ======================================= UTILITIES ==========================================
    // ============================================================================================

    /**
     * Concatenate the idnumbers for a list of users by delimiting them with '-'
     * @param users The list of users to process
     * @return A String with all the user's idnumber delimited by '-'
     * @apiNote The order of the idnumbers in the result is the same as the order in {@param users}
     */
    private String joinUsersIdNumber(final List<User> users) {
        return users.stream()
                .map(User::idnumber)
                .collect(Collectors.joining("-"));
    }

}

package ch.epfl.autograde.service;

import ch.epfl.autograde.model.entity.Registry;
import ch.epfl.autograde.properties.AutogradeJobConfig;

import ch.epfl.autograde.utils.KubernetesPreprocessor;
import io.fabric8.kubernetes.api.model.ObjectMetaBuilder;
import io.fabric8.kubernetes.api.model.SecretBuilder;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Map;

import static java.lang.String.format;
import static java.lang.String.valueOf;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.nonNull;

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
@Slf4j
@Service
@RequiredArgsConstructor
public final class KubernetesSecretService {

    private final KubernetesClient k8s;

    private final AutogradeService autograde;

    private final AutogradeJobConfig jobConfig;

    // ============================================================================================
    // ==================================== DOCKERCONFIGJSON ======================================
    // ============================================================================================

    public String credentials_secret_name(int assignmentid) {
        return format("regcred-%d", assignmentid);
    }

    /**
     * Check if the cluster stores the credentials secret for the given assignment
     *
     * @param assignmentid ???
     * @return ???
     */
    public boolean hasRegistryCredentialsSecret(int assignmentid) {
        return isSecretPresent(credentials_secret_name(assignmentid));
    }

    /**
     * Create the registry credentials secret on the cluster
     *
     * @param assignmentid assignment id
     * @param registry  credentials to use to pull the image
     */
    public void store_registry_credentials(int assignmentid, Registry registry) {
        var name = credentials_secret_name(assignmentid);

        var secret = new SecretBuilder()
                .withApiVersion("v1")
                .withKind("Secret")
                .withType("kubernetes.io/dockerconfigjson")
                .withMetadata(new ObjectMetaBuilder()
                        .withName(name)
                        .withLabels(Map.of(
                                "assignment.id", KubernetesPreprocessor.preprocessLabel(valueOf(assignmentid)),
                                "autograde.version", KubernetesPreprocessor.preprocessLabel(autograde.getVersion())
                        )).build()
                ).withData(Map.of(".dockerconfigjson", encode_secret(toDockerConfigJson(registry))))
                .build();
        // HR : Store the secret on the cluster
        try {
            var resource = k8s.secrets()
                    .inNamespace(jobConfig.namespace())
                    .resource(secret);

            // HR : Check for already existing secret and update if present
            if (hasRegistryCredentialsSecret(assignmentid)) {
                log.info("Updating secret with name '{}'", name);
                resource.update();
            } else {
                log.info("Creating secret with name '{}'", name);
                resource.create();
            }
        } catch (KubernetesClientException e) {
            log.error("Failed to create/update secret {}", name, e);
            throw new RuntimeException("Failed to store registry credentials");
        }
    }

    // ============================================================================================
    // =========================================== UTILS ==========================================
    // ============================================================================================

    /**
     * Check if a secret with the given name is present in the cluster
     *
     * @param name ???
     * @return true if present, false otherwise
     */
    public boolean isSecretPresent(String name) {
        try {
            log.info("Checking if the following secret is present in the cluster '{}'", name);
            var secret = k8s.secrets().inNamespace(jobConfig.namespace()).withName(name).get();
            return nonNull(secret);
        } catch (Exception e) {
            log.error("Failed trying to read existing secret {}", name, e);
            throw new RuntimeException("Failed trying to read existing imagePullSecret");
        }
    }

    private String encode_secret(String secret) {
        return Base64.getEncoder().encodeToString(secret.getBytes(UTF_8));
    }

    private void delete_secret(String name) {
        try {
            log.info("Deleting secret with name {}", name);
            k8s.secrets()
                    .inNamespace(jobConfig.namespace())
                    .withName(name)
                    .delete();
            log.info("Deleted secret with name '{}'", name);
        } catch (Exception e) {
            if (isSecretPresent(name)) {
                log.error("Failed trying to delete existing secret with name '{}'", name, e);
                throw new RuntimeException("Failed trying to delete existing imagePullSecret");
            } else {
                log.info("Secret with name '{}' was not found, nothing to delete", name);
            }
        }
    }

    private String toDockerConfigJson(Registry registry){
        return """
                {
                    "auths" : {
                        "%s" : {
                            "auth" : "%s"
                        }
                    }
                }
                """.formatted(registry.host(), encodeUsernameAndToken(
                        registry.credentials().username(),
                        registry.credentials().token()
                ));
    }

    private String encodeUsernameAndToken(String username, String token){
        var format = String.format("%s:%s", username, token);
        return Base64.getEncoder().encodeToString(format.getBytes(UTF_8));
    }

    public void delete_registry_credentials(int id) {
        delete_secret(credentials_secret_name(id));
    }
}

package ch.epfl.autograde.model.entity;

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
public record MoodleFile(
        String path,
        String filename,
        String mimetype,
        String content
) {}
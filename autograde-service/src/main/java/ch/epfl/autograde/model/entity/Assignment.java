package ch.epfl.autograde.model.entity;

public record Assignment(int id) { }

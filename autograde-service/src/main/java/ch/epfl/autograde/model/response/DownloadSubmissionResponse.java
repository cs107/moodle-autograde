package ch.epfl.autograde.model.response;

import ch.epfl.autograde.model.entity.MoodleFile;

import java.util.List;

public record DownloadSubmissionResponse(int id, List<MoodleFile> files) {}

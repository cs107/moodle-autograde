package ch.epfl.autograde.model.entity;

public record Course(int id, String shortName, String name) { }

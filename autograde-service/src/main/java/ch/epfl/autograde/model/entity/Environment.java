package ch.epfl.autograde.model.entity;

import java.util.Map;

public record Environment(String image, Map<String, String> variables) {
}

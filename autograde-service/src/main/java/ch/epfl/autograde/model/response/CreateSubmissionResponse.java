package ch.epfl.autograde.model.response;

import ch.epfl.autograde.controller.api.v1.SubmissionController;
import ch.epfl.autograde.model.request.CreateSubmissionRequest;

/**
 * Response to the {@link SubmissionController#create(CreateSubmissionRequest)} end-point
 * @param id The submission id as provided by Moodle
 * @param job The generated job name for the grading process
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
public record CreateSubmissionResponse(int id, String job) { }

package ch.epfl.autograde.model.request;

import ch.epfl.autograde.model.entity.*;

import java.util.List;

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
public record CreateSubmissionRequest(
        Submission submission,
        Assignment assignment,

        List<User> users,
        Course course,
        Environment environment
) { }
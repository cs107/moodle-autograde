package ch.epfl.autograde.model;

/**
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
public enum SubmissionStatus {
    WAITING,
    PULLING_SUBMISSION,
    GRADING,
    UPLOADING_FEEDBACK,
    COMPLETED,
    FAILED;
}

package ch.epfl.autograde.model.response;

import ch.epfl.autograde.controller.api.v1.PingController;

import java.util.Date;

/**
 * Response to the {@link PingController#ping_with_auth()  ping_with_auth} & {@link PingController#ping_without_auth()  ping_without_auth} end-points
 * @param time Timestamp indicating the execution time
 * @param auth flag to check if the ping was authenticated
 */
public record PingResponse(Date time, boolean auth) {}

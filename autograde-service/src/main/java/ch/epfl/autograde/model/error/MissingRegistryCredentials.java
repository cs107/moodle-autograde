package ch.epfl.autograde.model.error;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
@RequiredArgsConstructor
public final class MissingRegistryCredentials extends RuntimeException {

    private final int id;

    public int id() {
        return id;
    }

}

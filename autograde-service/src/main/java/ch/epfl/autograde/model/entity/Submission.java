package ch.epfl.autograde.model.entity;

public record Submission(int id, int attempt) {
}

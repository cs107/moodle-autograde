package ch.epfl.autograde.model.response;

import ch.epfl.autograde.model.entity.User;

import java.util.List;

public record SubmissionInfoResponse(int sid, int aid, int cid, List<User> submitters, int attempt) {

}

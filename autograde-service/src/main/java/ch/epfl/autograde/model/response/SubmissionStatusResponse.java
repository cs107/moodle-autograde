package ch.epfl.autograde.model.response;

import ch.epfl.autograde.model.SubmissionStatus;

public record SubmissionStatusResponse(int id, SubmissionStatus status) { }

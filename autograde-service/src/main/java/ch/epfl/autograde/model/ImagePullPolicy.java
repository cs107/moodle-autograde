package ch.epfl.autograde.model;

/**
 *
 * @since 1.1.0
 * @see <a href="https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy">Kubernetes Official Documentation</a>
 */
public enum ImagePullPolicy {
    Never,
    Always,
    IfNotPresent
}

package ch.epfl.autograde.model.response;

public record InfoResponse(String url, String version) {

}

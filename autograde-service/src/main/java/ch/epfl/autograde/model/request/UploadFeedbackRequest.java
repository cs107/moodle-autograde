package ch.epfl.autograde.model.request;

import ch.epfl.autograde.model.entity.MoodleFile;

import java.util.List;

public record UploadFeedbackRequest (
        float grade,
        List<MoodleFile> files
) { }

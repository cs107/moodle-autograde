package ch.epfl.autograde.model.entity;

public record RegistryCredentials(String username, String token) {
}

package ch.epfl.autograde.model.entity;

public record User(int id,String username, String firstname,String lastname,String idnumber,String email) {
}

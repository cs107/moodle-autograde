package ch.epfl.autograde.model;

import java.util.Base64;
import java.nio.charset.StandardCharsets;

public record BasicAuth(String username, String password) {

    public String toHeaderToken() {
        var fmt = String.format("%s:%s", username, password);
        var base64 = Base64.getEncoder().encodeToString(fmt.getBytes(StandardCharsets.UTF_8));
        return String.format("Basic %s", base64);
    }

}

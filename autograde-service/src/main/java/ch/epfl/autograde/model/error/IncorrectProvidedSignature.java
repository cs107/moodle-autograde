package ch.epfl.autograde.model.error;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.BAD_REQUEST)
@RequiredArgsConstructor
public final class IncorrectProvidedSignature extends RuntimeException {

    private final int id;

    private final String provided;

    public int id() {
        return id;
    }

    public String provided() {
        return provided;
    }

}

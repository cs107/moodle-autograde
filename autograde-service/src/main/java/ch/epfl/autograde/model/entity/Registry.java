package ch.epfl.autograde.model.entity;

public record Registry(String host, RegistryCredentials credentials) {
}

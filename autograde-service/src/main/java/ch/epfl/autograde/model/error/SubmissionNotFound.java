package ch.epfl.autograde.model.error;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
@RequiredArgsConstructor
public final class SubmissionNotFound extends RuntimeException {

    private final int id;

    private final String reason;

    public int id() {
        return id;
    }

    public String reason(){
        return reason;
    }

}

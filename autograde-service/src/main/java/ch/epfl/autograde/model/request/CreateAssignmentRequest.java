package ch.epfl.autograde.model.request;

import ch.epfl.autograde.model.entity.Assignment;
import ch.epfl.autograde.model.entity.Course;
import ch.epfl.autograde.model.entity.Registry;

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
public record CreateAssignmentRequest(
        Assignment assignment,
        Course course,
        Registry registry
) {}

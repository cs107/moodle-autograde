package ch.epfl.autograde;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

/**
 * ???
 *
 * @author Hamza REMMAL (hamza.remmal@epfl.ch)
 */
@SpringBootApplication
@ConfigurationPropertiesScan
public class AutogradeServiceApplication {

	/**
	 * ???
	 * @param args ???
	 */
	public static void main(String[] args) {
		SpringApplication.run(AutogradeServiceApplication.class, args);
	}

}

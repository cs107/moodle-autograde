# Moodle Administrator Guide

This document describes the steps to configure the Moodle instance to use the autograde plugin. It is only targeted at Moodle *administrators*, not teachers.

## Enable Web services

- Go to `Site Administration > Advanced Features`: `Enable web services` should be checked
- Go to `Site Administration > Server > Manage Protocols`: Make sure that the `REST protocol` is enabled

## Create the bot user

To communicate with the Moodle API, we create a new web service user.

- Go to `Site Administration > Users > Add a new user`:
  - username: *autograde-bot*
  - authentication method: *Web services authentication*
  - first name: *Autograde*
  - surname: *Bot*
  - email address: *autograde-support@groupes.epfl.ch*
  - email visibility: *Hidden*

## Setup the required capabilities

Now that we have our user account, we will have to provide some capabilities to that user. We will define 2 custom roles, each one of them
in a different level (or context).

### A system level role

To be able to make API requests to Moodle, the autograde user will need to have the necessary capability at the system level.

- Go to Site `Administration > Users > Define roles > Add a new role`
- No need to select an archetype
- short name: *autograde-system-role*
- custom full name: *autograde-system*
- context: Select ***`System`***
- add the following capability: 
  - `webservice/rest:use`

**Note:** if a similar role is already defined, we don't need to create it again.

### A course level role

Similarly to the previous section, we will now define a *course-level* role. 

- Go to `Site Administration > Users > Define roles > Add a new role`
- No need to select an archetype
- short name: *autograde-course-role*
- Custom full name: *autograde-course*
- Context: Select ***`Course`*** 
- Add the following capabilities: 
  - `assignsubmission/autograde:viewsubmissions`
  - `assignsubmission/autograde:viewsubmission`
  - `assignsubmission/autograde:exportsubmissions`
  - `assignsubmission/autograde:grade`
  - `assignsubmission/autograde:use`
  - `mod/assign:grade`
  - `moodle/course:viewhiddencourses`

### Assign the roles to the user

Now that the roles have been created, we need to assign the roles to the bot user.

- Assign the *autograde-system-role* role to the *autograde-bot* user:
    - Go to `Site Administration > Users > Assign system roles`
    - Select *autograde-system* and add the corresponding user to the list
- Assign the *autograde-course-role* in all the necessary courses

### Authorise the bot to use the web service

The Moodle plugin defines a Moodle webservice named *AUTOGRADE*. This webservice is used to communicate with the autograde instance. Most of the work has done in the plugin's code, we only need to allow the autograde user to use the service.

- Go to `Site Administration > Server > External services`
- Select `Authorised Users` in the *AUTOGRADE* service and add your autograde bot user

## Configure the communication between Moodle and Autograde

As a last step, we should configure the communication between the Moodle instance and its corresponding autograde instance.

### Set the service URL

Configure the communication from *Moodle* to *Autograde* by going to `Site Administration > Plugins > Autograde`: 
- Add the provided `Service URL`.
- Add the provided `API Key`.

> For local development, the values of these options are respectively `http://192.168.1.100:8082` and `12345`, corresponding to the `AUTOGRADE_BASEURL` and `AUTOGRADE_API_KEY` enviroment varailbles in [docker-compose.yaml](../../docker-compose.yaml).

### Create the Moodle access token

- Go to `Site Administration > Server > Manage Tokens > Create Token`
- Select your autograde bot user as the user
- Select *AUTOGRADE* as the service
- Provide the *access token* and the *Moodle Base URL* to the autograde admins.

> For local development, the environment variable `AUTOGRADE_MOODLE_TOKEN` should be set to the access token generated above, and the docker compose containers should be restarted.
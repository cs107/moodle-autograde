# Local Development Setup

The following instructions guide you through the installation and configuration of a local environment to develop and experiment with the Moodle autograde infrastructure. 

## Installation

### Step 0: Required tools 

The following tools are required for local experimentation:
- [docker](https://docs.docker.com/engine/install/), the Docker containers runtime 
- [make](https://www.gnu.org/software/make/manual/make.html), the build automation tool 
- [git](https://git-scm.com/), the distributed version control system

You will first have to clone the project repository with its dependencies
```
git clone git@gitlab.epfl.ch:cs107/moodle-autograde.git
```

### Step 1: Start The Services

Before starting the services, you will have to make sure that the [Docker Engine is running](https://docs.docker.com/config/daemon/start/).
If not, please start the *Docker daemon* before running the following command:
```
docker compose up --build
```

> Note that it can take a few minutes after running the command for the services to be fully deployed.

### Step 2: Configuring Your Local Domain Names

Link the local instance of Moodle with the host `moodle`.
This is done by adding the line `127.0.0.1     moodle` to [/etc/host](https://en.wikipedia.org/wiki/Hosts_(file)):
```
make add-host
```

### Step 3: Access The Services 

The local instance of Moodle is accessible at http://moodle.

The autograde service is reachable at http://localhost:8082/api/v1/ping.

### Step 4: Moodle Installation

At this point, you should see the following webpage when accessing http://moodle:

![moodle installation](./images/moodle_start_installation.png)

Go through the [installation step](https://docs.moodle.org/404/en/Installing_Moodle#Web_based_installer).
You can enter whatever value you want for the required fields.

### Step 5: Moodle Configuration

Now that Moodle is installed, follow the instruction in [moodle_config.md](./moodle_config.md) to configure the Moodle instance.

At this point, you should have a local instance of the Moodle autograde cluster configured and running. 

### Step 6: To Go Further

Now that a local instance of the autograde cluster is set up,
you can experiment with creating classes and auto-graded homeworks.

## Reset the local host configuration

You can remove the mapping from the `moodle` host name with:
```
make rm-host
```

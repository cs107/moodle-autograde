#!/usr/bin/env python3

import argparse
import base64
import collections
import csv
from datetime import datetime
from typing import Dict, List, Tuple

import matplotlib.pyplot as plt
from kubernetes import client, config

IMAGE_PULL_SECRET_NAME = "loadtest-regcred"
JOB_NAME_FORMAT = "loadtest-{}"

JOB_RESOURCES = {"memory": "2Gi", "cpu": "1000m"}


def create_job_object(name: str, image: str) -> client.V1Job:
    """
    Create a new job object
    """
    # Container definition
    container = client.V1Container(
        name=name,
        image=image,
        image_pull_policy="IfNotPresent",
        resources=client.V1ResourceRequirements(
            requests=JOB_RESOURCES,
            limits=JOB_RESOURCES,
        ),
    )

    # Spec for container
    template_spec = client.V1PodSpec(
        containers=[container],
        restart_policy="Never",
        image_pull_secrets=[client.V1LocalObjectReference(name=IMAGE_PULL_SECRET_NAME)],
    )

    # Spec for job template
    job_template_spec = client.V1PodTemplateSpec(
        metadata=client.V1ObjectMeta(labels={"app": "loadtest"}),
        spec=template_spec,
    )

    # Spec for job
    job_spec = client.V1JobSpec(
        template=job_template_spec,
        backoff_limit=2,
    )

    # Job definition
    job = client.V1Job(
        api_version="batch/v1",
        kind="Job",
        metadata=client.V1ObjectMeta(name=name),
        spec=job_spec,
    )

    return job


def create_image_pull_secret(regcred_filename: str) -> client.V1Secret:
    with open(regcred_filename, "r") as f:
        docker_config_json = f.read()

    secret = client.V1Secret(
        api_version="v1",
        kind="Secret",
        metadata=client.V1ObjectMeta(name=IMAGE_PULL_SECRET_NAME),
        type="kubernetes.io/dockerconfigjson",
        data={
            ".dockerconfigjson": base64.b64encode(docker_config_json.encode()).decode()
        },
    )

    return secret


def start_jobs(namespace: str, n: int, image: str, regcred_filename: str):
    core_v1 = client.CoreV1Api()

    # Check if image pull secret already exists
    secret_list: client.V1SecretList = core_v1.list_namespaced_secret(
        namespace=namespace,
        field_selector="metadata.name={}".format(IMAGE_PULL_SECRET_NAME),
    )

    # If there is an existing secret, delete it
    if secret_list.items:
        print("Deleting existing secret.")
        core_v1.delete_namespaced_secret(
            name=IMAGE_PULL_SECRET_NAME, namespace=namespace
        )

    # Create a new secret
    core_v1.create_namespaced_secret(
        namespace=namespace, body=create_image_pull_secret(regcred_filename)
    )

    batch_v1 = client.BatchV1Api()

    # Check if jobs already exist
    job_list: client.V1JobList = batch_v1.list_namespaced_job(
        namespace=namespace, label_selector="app=loadtest"
    )

    # If there are existing jobs, notify the user to run "cleanup"
    if job_list.items:
        print("There are existing jobs. Please run `python loadtest.py cleanup` first.")
        return

    # Create jobs
    print(f"Creating {n} jobs with image {image}.")
    for i in range(n):
        job_name = JOB_NAME_FORMAT.format(i)
        batch_v1.create_namespaced_job(
            namespace=namespace, body=create_job_object(job_name, image)
        )


def get_job_conditions(namespace: str):
    batch_v1 = client.BatchV1Api()

    # Get jobs
    print("Fetching job conditions.")
    job_list: client.V1JobList = batch_v1.list_namespaced_job(
        namespace=namespace, label_selector="app=loadtest"
    )

    conditions_table: Dict[str, int] = collections.defaultdict(int)

    # Get job conditions
    job: client.V1Job
    for job in job_list.items:
        status: client.V1JobStatus = job.status
        conditions: List[client.V1JobCondition] = status.conditions
        if not conditions:
            conditions_table["Pending"] += 1
        else:
            condition: client.V1JobCondition
            for condition in conditions:
                if condition.status == "True":
                    conditions_table[condition.type] += 1

    # Print job conditions
    print("Job conditions:")
    for condition, count in conditions_table.items():
        print(f"\t{condition}: {count}")


def plot_job_times(namespace: str):
    batch_v1 = client.BatchV1Api()

    # Get jobs
    print("Fetching job data.")
    job_list: client.V1JobList = batch_v1.list_namespaced_job(
        namespace=namespace, label_selector="app=loadtest"
    )

    # For each job, store the start time and completion time
    job_times: List[Tuple[float, float]] = []

    # Get job times
    job: client.V1Job
    for job in job_list.items:
        status: client.V1JobStatus = job.status
        if status.completion_time is not None:
            start_time: datetime = status.start_time
            end_time: datetime = status.completion_time
            job_times.append(
                (
                    start_time.timestamp(),
                    end_time.timestamp(),
                )
            )

    # Write job times to csv
    print("Writing job times to csv.")
    with open("job_times.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerow(["start", "end"])
        writer.writerows(job_times)

    # Convert job times to minutes
    job_times_minutes: List[Tuple[float, float]] = [
        (start / 60, end / 60) for start, end in job_times
    ]

    # Sort job times by end time
    job_times_minutes.sort(key=lambda x: x[1])

    # Plot job times
    print("Plotting job times.")
    fig, ax = plt.subplots(figsize=(10, len(job_times_minutes)))

    # Each job is represented by a horizontal bar
    y_vals = range(len(job_times_minutes))
    for idx, (start, end) in enumerate(job_times_minutes):
        ax.barh(y_vals[idx], end - start, left=start, color="blue")

    ax.set_yticks(y_vals)
    ax.set_yticklabels([f"Job {i}" for i in range(len(job_times_minutes))])
    ax.set_xlabel("Time (minutes)")
    ax.set_title("Time for all jobs to reach completion")

    # Set x-axis limits
    min_time = min([start for start, _ in job_times_minutes])
    max_time = max([end for _, end in job_times_minutes])
    ax.set_xlim(min_time, max_time)

    plt.tight_layout()
    plt.savefig("gantt_chart.png")


def delete_resources(namespace: str):
    # Delete jobs
    print("Deleting jobs.")
    batch_v1 = client.BatchV1Api()
    batch_v1.delete_collection_namespaced_job(
        namespace=namespace, label_selector="app=loadtest"
    )

    # Delete image pull secret
    print("Deleting image pull secret.")
    core_v1 = client.CoreV1Api()
    core_v1.delete_namespaced_secret(name=IMAGE_PULL_SECRET_NAME, namespace=namespace)


def main():
    parser = argparse.ArgumentParser(description="Manage Kubernetes jobs.")

    # Create subparsers for each command
    subparsers = parser.add_subparsers(dest="command")

    # Define the 'start' command
    start_parser = subparsers.add_parser("start", help="Start jobs.")
    start_parser.add_argument("n", type=int, help="Number of jobs to start.")
    start_parser.add_argument("image", type=str, help="Docker image tag to use.")
    start_parser.add_argument("regcred", type=str, help="Docker registry credentials.")

    # Define the 'status' command
    status_parser = subparsers.add_parser("status", help="Get status of jobs.")

    # Define the 'plot' command
    plot_parser = subparsers.add_parser("plot", help="Plot job times.")

    # Define the 'cleanup' command
    cleanup_parser = subparsers.add_parser("cleanup", help="Clean up jobs.")

    args = parser.parse_args()

    # Load the Kubernetes configuration
    config.load_kube_config()
    _, active_context = config.list_kube_config_contexts()
    namespace = active_context["context"].get("namespace", "default")

    if args.command == "start":
        start_jobs(namespace, args.n, args.image, args.regcred)
    elif args.command == "status":
        get_job_conditions(namespace)
    elif args.command == "plot":
        plot_job_times(namespace)
    elif args.command == "cleanup":
        delete_resources(namespace)


if __name__ == "__main__":
    main()

**_Load-test the nodes of a cluster with 'n' jobs started at the same time, each running the same executable container _**

---

1. Create a new Conda environment with the required packages:

   ```
   conda env create -f environment.yml
   ```

2. Activate the environment:

   ```
   conda activate k8s-loadtest
   ```

3. Run the load-test:

   1. Start the load-test with three parameters;
      - the number of jobs to start,
      - the name of the executable container to run, and
      - the location of a .dockerconfigjson file containing the credentials to access the container registry.

   ```
   python3 load-test.py start 100 ic-registry.epfl.ch/autograde/sample-job:latest .dockerconfigjson
   ```

   2. Check the status of the jobs:

   ```
    python3 load-test.py status
   ```

   3. Plot the time taken by each job:

   ```
   python3 load-test.py plot
   ```

   3. Delete the jobs:

   ```
   python3 load-test.py cleanup
   ```

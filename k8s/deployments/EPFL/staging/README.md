## Configuration and deployment to EPFL's staging namespace

---

> Access to the configuration of EPFL's namespaces is restricted to the project admins.
>
> To contact `autograde-admins`, please use the following mail:
> [autograde-admins@groupes.epfl.ch](mailto:autograde-admins@groupes.epfl.ch)
> or directly contact [Hamza REMMAL](mailto:hamza.remmal@epfl.ch).

1. Go to the [`secrets`](secrets) subdirectory and configure all the secrets.
   The necessary files to configure are:
   - `autograde-secrets.env`

2. Make sure that the configuration in the [`config`](config) folder is up-to-date.
   If not, please commit any changes to make sure that the configuration is synchronized.

3. Make sure you have added the necessary configuration to access the namespace.
   If not, you should add the `kubeconfig` file to the root of [this folder](.).

4. To deploy/update EPFL's production instance of autograde, use the following command:
   ```bash
   kubectl apply -k k8s/deploy-envs/cluster/staging-epfl
   ```

5. To restart any of the autograde running instances, use the following commands:
   - For `autograde-service`: `kubectl rollout restart deployment/autograde-service`

> Access to the services is possible within EPFL's network via the following URLs
>
> - For the *API* of the running instance: https://moodle-autograde-test.epfl.ch/api

### Deployment to EPFL's staging

To deploy, update and restart the services in EPFL's staging environment,
have a look at the provided [Makefile](Makefile).

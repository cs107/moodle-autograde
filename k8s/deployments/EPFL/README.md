## Deployment of autograde for EPFL

---

> Access to EPFL's configuration of autograde is restricted to the project admins.
>
> To contact `autograde-admins`, please use the following mail:
> [autograde-admins@groupes.epfl.ch](mailto:autograde-admins@groupes.epfl.ch)
> or directly contact [Hamza REMMAL](mailto:hamza.remmal@epfl.ch).


This folder contains the configurations of autograde for EPFL.
- [prod](prod) contains the configuration of the production environment
- [staging](staging) contains the configuration of the staging environment

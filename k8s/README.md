# Local Development Setup

## Prerequisites

- Install [minikube](https://minikube.sigs.k8s.io/docs/start/) for local Kubernetes cluster
- Install [Docker](https://docs.docker.com/get-docker/) for use as the minikube driver
- Install [kubectl](https://kubernetes.io/docs/tasks/tools/) for interacting with the Kubernetes cluster

## Local Setup Commands

| Command                           | Description                                                      | When to Run                                                            |
|-----------------------------------|------------------------------------------------------------------|------------------------------------------------------------------------|
| `make start-minikube`             | Start minikube and set the context                               | Start of every new session                                             |
| `make set-context`                | Set kubectl context                                              | If the context gets changed and needs to be set again                  |
| ➡️ `make dashboard`               | Open Kubernetes dashboard                                        | To visually inspect the cluster status                                 |
| ➡️ `make start-tunnel`            | Start minikube tunnel to access the services                     | Every time services need to be accessed after starting minikube        |
| `make build-autograde`            | Build autograde-service image                                    | After changes in grading service code                                  |
| `make build-moodle`               | Build Moodle image                                               | After changes in Moodle or autograder-plugin code                      |
| `make deploy`                     | Deploy the environment                                           | After initial setup or any changes in deployment configuration         |
| `make restart-autograde`          | Restart autograde-service deployment                             | After changes in grading service image                                 |
| `make restart-moodle`             | Restart Moodle deployment                                        | After changes in Moodle image                                          |
| `make update-grading-service-env` | Update grading-service environment variables                     | After changes in `k8s/deploy-envs/local/grading-service-variables.env` |
| `make stop-minikube`              | Stop Kubernetes cluster and minikube but keep the persisted data | When you're done for the day/session                                   |
| `make add-hosts`                  | Add moodle-tcp to /etc/hosts                                     | Initially, if you want to access the services via domain name          |
| `make destroy-minikube`           | Shut down the Kubernetes cluster and wipe all data               | When you want to start fresh                                           |

➡️ Run these commands in a separate terminal window because they block the current terminal session.

## Instructions for Setting Up dotenv File

Please add the following files to the `/deploy-envs/local` with the correct values. 
- `.dockerconfigjson` : Give access to your personal Docker repository in case you intend to pull the images from it.
- `service-configuration.env` : Configure the grading-service to communicate with your local moodle instance
This file has the following structure
```env
API_KEY=<ADD YOUR SERVICE API KEY>
MOODLE_BASEURL=http://moodle-tcp:80
MOODLE_AUTOGRADE_TOKEN=<ADD YOUR MOODLE ACCESS TOKEN>
GRADING_SERVICE_NAME=grading-service-tcp
```

## Accessing the Services

- Access Moodle via http://moodle-tcp:80
- Access the Moodle database via http://localhost:3304 (username: moodle, password: moodle)
- Access the grading-service Spring app via http://localhost:8082 (ping endpoint: /api/v1/ping/no-auth)
